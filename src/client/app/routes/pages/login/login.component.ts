import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ActivatedRoute } from '@angular/router';
import { Employee } from 'app/core/data-services/employee';
import 'rxjs/add/operator/toPromise';
import { Router } from '@angular/router';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { MenuService } from 'app/core/menu/menu.service';

import { menu } from '../../menu'
import { Customer } from 'app/core/data-services/customer';
import { CustomerService } from 'app/core/data-services/customer.service';
import { NavigationExtras } from '@angular/router/src/router';

function loginMethodValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select Login Method') {
            return { 'notSelected': true };
        };
        return null;
    };
}

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    valForm: FormGroup;
    public user: any
    eList: Employee[] = []
    cList: Customer[] = []
    private methods = ['Select Login Method', 'Customer', 'Internal']

    private loginUser: Employee
    private loginCustomer: Customer

    constructor(public settings: SettingsService, 
                private customerService: CustomerService, 
                fb: FormBuilder, 
                public userBlockService: UserblockService, 
                private route: ActivatedRoute, 
                private router: Router, 
                public menuService: MenuService) {

        this.valForm = fb.group({
            'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'password': [null, Validators.required],
            'loginMethod': ['', loginMethodValidator()]
        });
    }

    submitForm($ev, value: any) {

        let message = 'Cannot Log-in!'
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
            if (this.valForm.get('loginMethod').value == "Internal") {
                for (let i = 0; i < this.eList.length; i++) {
                    if (this.eList[i].email == this.valForm.get('email').value && this.eList[i].password === this.valForm.get('password').value) {
                        this.loginUser = this.eList[i]
                        message = 'Successfully Logged In!'
                    }
                }
                alert(message)
                if (message == 'Successfully Logged In!') {
                    this.user = {
                        id: this.loginUser.id,
                        name: this.loginUser.firstName,
                        role: this.loginUser.userRole,
                        picture: 'assets/img/user/03.jpg'
                    };
                    this.userBlockService.setUserData(this.user)
                    console.log(this.route.snapshot.url[0].path)
                    this.menuService.addMenu(menu)
                    this.router.navigate(['/dashboard/v1'])
                }
            } else if (this.valForm.get('loginMethod').value == "Customer") {
                for (let i = 0; i < this.cList.length; i++) {
                    if (this.cList[i].email == this.valForm.get('email').value && 
                        this.cList[i].password === this.valForm.get('password').value) {
                        this.loginCustomer = this.cList[i]
                        message = 'Successfully Logged In!'
                    }
                }
                alert(message)
                if (message == 'Successfully Logged In!') {
                    this.user = {
                        id: this.loginCustomer.id,
                        name: this.loginCustomer.name,
                        role: 'Customer',
                        picture: 'assets/img/user/03.jpg'
                    };
                    this.userBlockService.setUserData(this.user)
                    this.menuService.addMenu(menu)
                    this.router.navigate(['/home'])
                }
            }
        }
    }

    ngOnInit() {
        this.route.data
            .subscribe(params => {
                let list: Employee[] = params['user']
                this.eList = list
            });
        this.route.data
            .subscribe(params => {
                let list: Customer[] = params['customer']
                this.cList = list
            });
    }
}
