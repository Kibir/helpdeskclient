import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { CustomerService } from 'app/core/data-services/customer.service';
import { Customer } from 'app/core/data-services/customer';
import { Router } from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    valForm: FormGroup;
    passwordForm: FormGroup;
    customer:Customer = new Customer()

    constructor(public settings: SettingsService, fb: FormBuilder, private customerService:CustomerService, private route:Router) {

        let password = new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9]{6,10}$')]));
        let certainPassword = new FormControl('', CustomValidators.equalTo(password));

        this.passwordForm = fb.group({
            'password': password,
            'confirmPassword': certainPassword
        });

        this.valForm = fb.group({
            'name':[null, Validators.required],
            'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'company':[null, Validators.required],
            'accountagreed': [null, Validators.required],
            'passwordGroup': this.passwordForm
        });
    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        for (let c in this.passwordForm.controls) {
            this.passwordForm.controls[c].markAsTouched();
        }

        if (this.valForm.valid) {
            console.log('Valid!');
            console.log(value);
            this.customer.id = 3
            this.customer.name = this.valForm.get('name').value
            this.customer.company = this.valForm.get('company').value
            this.customer.email = this.valForm.get('email').value
            this.customer.password = this.passwordForm.get('password').value
            console.log('Customer Account Created!'+ this.customer)
            this.customerService.saveCustomer(this.customer).subscribe(() => console.log('Customer Account Created!'+ this.customer))
            alert('Account Successfully Created! You can log-in as a customer now!')
            this.route.navigate(['login'])
        }
    }

    ngOnInit() {
    }

}
