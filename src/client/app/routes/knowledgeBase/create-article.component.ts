import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Article } from 'app/core/data-services/article';
import { Reference } from 'app/core/data-services/reference';
import { ArticleService } from 'app/core/data-services/article.service';

@Component({
    selector: 'create-article',
    templateUrl: './create-article.component.html'
})

export class CreateArticleComponent implements OnInit{
    createArticleForm:FormGroup
    private tempId:number = 9
    isUserLoggedIn:boolean = false
    isUserAdmin: boolean = false
    errorMessage:any
    
    @Output() newArticleCreated:EventEmitter<string> = new EventEmitter<string>();
  
    constructor(public userBlockService:UserblockService, private fb:FormBuilder, private articleService:ArticleService){
        
    }
    ngOnInit() {
        this.createArticleForm = this.fb.group({
            title: ['', [Validators.required, Validators.maxLength(55)]] ,
            body: ['', [Validators.required, Validators.minLength(100)]] ,
            conclusion:['', [Validators.required, Validators.minLength(50)]] ,
            references:this.fb.array([this.buildReferences()]),
        })
    }

    

    get references(): FormArray{
        return <FormArray>this.createArticleForm.get('references');
    }

    
    addReferences(): void {
        this.references.push(this.buildReferences());
    }

    buildReferences(): FormGroup {
        return this.fb.group({
            articleId:[this.tempId],
            refName:'',
            refLink:''
        });
    }

    createArticle(){
        let newArticle:Article = new Article()

        newArticle.id = this.tempId
        newArticle.title = this.createArticleForm.get('title').value
        newArticle.body = this.createArticleForm.get('body').value
        
        newArticle.conclusionBody = this.createArticleForm.get('conclusion').value
        newArticle.references = this.createArticleForm.get('references').value
        newArticle.lastUpdatedDate = new Date().toDateString()
        newArticle.postedDate = new Date().toDateString()
        newArticle.postedUserId = this.userBlockService.getUserData().id
        newArticle.postedUserName = this.userBlockService.getUserData().name
    

        //newArticle.body = newArticle.body.replace(/(\r\n|\r|\n)/g, '<br/>')
        //.replace(/\t/g, '&nbsp;&nbsp;&nbsp;')        
        //.replace(/ /g, '&nbsp;');
        //newArticle.conclusionBody = newArticle.conclusionBody.replace(/(\r\n|\r|\n)/g, '<br/>')
       // .replace(/\t/g, '&nbsp;&nbsp;&nbsp;')        
        //.replace(/ /g, '&nbsp;');
        this.articleService.saveArticle(newArticle).subscribe(() => this.afterCreatingNewArticle())
    }

    afterCreatingNewArticle(){
        this.newArticleCreated.emit('New Article Created!')
    }

    closeModal(){
        this.createArticleForm.reset()
    }

    fileChange(event) {
        
         let fileList: FileList = event.target.files;
         
         if(fileList.length > 0) {
             this.articleService.uploadFile(fileList)
         }
     }

    
    
}