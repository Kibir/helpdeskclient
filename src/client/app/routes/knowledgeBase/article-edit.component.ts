import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Article } from 'app/core/data-services/article';
import { Reference } from 'app/core/data-services/reference';
import { ArticleService } from 'app/core/data-services/article.service';
declare var tinymce:any
@Component({
    selector: 'edit-article',
    templateUrl: './article-edit.component.html'
})

export class EditArticleComponent implements OnInit{
    editArticleForm:FormGroup
    //private tempId:number = 9
    isUserLoggedIn:boolean = false
    isUserAdmin: boolean = false
    private initialized:boolean = false
    errorMessage:any
    @Input() articleToBeEdited:Article
    @Output() articleEdited:EventEmitter<string> = new EventEmitter<string>();

    private editedArticle:Article
    private referencesArray: FormArray;
  
    constructor(public userBlockService:UserblockService, private fb:FormBuilder, private articleService:ArticleService){
        
    }
    ngOnInit() {
        this.editArticleForm = this.fb.group({
            title: ['', [Validators.required, Validators.maxLength(55)]] ,
            body: ['', [Validators.required, Validators.minLength(100)]] ,
            conclusion:['', [Validators.required, Validators.minLength(50)]] ,
            references:this.fb.array([]),
        })
        
        
        this.initialized = true
        this.referencesArray = <FormArray>this.editArticleForm.controls['references'];
        
       
        
    }

    ngOnChanges(){
        if(this.initialized){
          
            this.fillFormData()
        }
     
    }

    fillFormData(){
        this.editedArticle = this.articleToBeEdited
        this.editArticleForm.patchValue({
            title: [this.editedArticle.title] ,
            body: [this.editedArticle.body] ,
            conclusion:[this.editedArticle.conclusionBody],
        });
        tinymce.get("bodyEditor").setContent(this.editedArticle.body);
        tinymce.get("conclusionEditor").setContent(this.editedArticle.conclusionBody);
        this.editedArticle.references.forEach(reference => {
                    const fb = this.buildReferences();
                    fb.patchValue(reference);
                    this.referencesArray.push(fb);
            });
    }

    get references(): FormArray{
        return <FormArray>this.editArticleForm.get('references');
    }

    
    addReferences(): void {
        this.references.push(this.buildReferences());
    }

    buildReferences(): FormGroup {
        return this.fb.group({
            articleId:[this.editedArticle.id],
            refName:'',
            refLink:''
        });
    }

    editArticle(){
        

        
        this.editedArticle.title = this.editArticleForm.get('title').value
        this.editedArticle.body = this.editArticleForm.get('body').value
        
        this.editedArticle.conclusionBody = this.editArticleForm.get('conclusion').value
        this.editedArticle.references = this.editArticleForm.get('references').value
        this.editedArticle.lastUpdatedDate = new Date().toDateString()
        this.editedArticle.postedDate = new Date().toDateString()
        this.editedArticle.postedUserId = this.userBlockService.getUserData().id
        this.editedArticle.postedUserName = this.userBlockService.getUserData().name
    

        //this.editedArticle.body = this.editedArticle.body.replace(/(\r\n|\r|\n)/g, '<br/>')
        //.replace(/\t/g, '&nbsp;&nbsp;&nbsp;')        
        //.replace(/ /g, '&nbsp;');
        //this.editedArticle.conclusionBody = this.editedArticle.conclusionBody.replace(/(\r\n|\r|\n)/g, '<br/>')
        //.replace(/\t/g, '&nbsp;&nbsp;&nbsp;')        
        //.replace(/ /g, '&nbsp;');
        this.articleService.updatingArticle(this.editedArticle).subscribe(() => this.afterEditingArticle())
    }

    afterEditingArticle(){
        this.articleEdited.emit('Article Edited!')
        this.closeModal()
    }

    closeModal(){
        this.editArticleForm.reset()
        while (this.referencesArray.length) {
            this.referencesArray.removeAt(0);
        }
        this.editArticleForm.patchValue({
            title: [this.editedArticle.title] ,
            body: [this.editedArticle.body] ,
            conclusion:[this.editedArticle.conclusionBody],
        });
                 
        this.editedArticle.references.forEach(reference => {
                    const fb = this.buildReferences();
                    fb.patchValue(reference);
                    this.referencesArray.push(fb);
            });
    }

}