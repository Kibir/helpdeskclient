import { Component, OnInit } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute, Router } from '@angular/router';

import { ArticleService } from 'app/core/data-services/article.service';
import { Article } from 'app/core/data-services/article';
import { PagerService } from 'app/core/data-services/pager.service';

declare var $:any;

@Component({
    selector: 'display-knowledgeSearch',
    templateUrl: './knowledge-results.component.html',

})

export class KnowledgeSearchDislayComponent implements OnInit{
   
    isUserLoggedIn:boolean = false
    pager: any = {};
    pagedItems: Article[];

    errorMessage:any

    articleList:Article[]

    constructor(public userBlockService:UserblockService, 
        private articleService:ArticleService, 
        private route:ActivatedRoute, 
        private _router:Router,
        private pagerService:PagerService){ 
    }
    ngOnInit(): void {
      let searchData = this.route.snapshot.paramMap.get('sd')
      this.search(searchData)
    }

     search(sd:string){
        this.articleService.getArticles()
        .map(articles => articles.filter(article => article.title.toLocaleLowerCase().indexOf(sd.toLocaleLowerCase()) >= 0))
        .subscribe(articles => {this.articleList = articles
            this.setPage(1)},
                   error => this.errorMessage = <any>error);
    } 

    summarizeData(data:string): string {
        if(data.length <= 116){
            return data
        }else{
            return data.substring(0,116).trim() + "..."
        }       
    }

    articleClick(article:Article){1
        this._router.navigate(['/knowledgeBase/detail',{id:article.id}])
    }
   
    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
    
        this.pager = this.pagerService.getPager(this.articleList.length, page);
        
        this.pagedItems = this.articleList.slice(this.pager.startIndex, this.pager.endIndex + 1);
       
    }

   
}