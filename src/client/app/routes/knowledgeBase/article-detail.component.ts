import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms/';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { ArticleService } from 'app/core/data-services/article.service';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'app/core/data-services/article';
declare var $:any
@Component({
    selector: 'article-detail',
    templateUrl: './article-detail.component.html'
    
})

export class ArticleDetailComponent implements OnInit{
    
    isUserLoggedIn:boolean = false
    errorMessage:any
    private specArticle:Article
    private articleId:number

    constructor(private userBlockService:UserblockService, private articleService:ArticleService,private route:ActivatedRoute, private fb:FormBuilder){
    }

    ngOnInit(){
         this.articleId = Number.parseInt(this.route.snapshot.paramMap.get('id'))
        
        if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
            this.isUserLoggedIn = true
        } 
        this.loadArticle()
    }

    loadArticle(){
        this.articleService.getArticle(this.articleId).subscribe(article => this.specArticle = article,()=> console.log(this.specArticle))
    }

    articleEdited(){
        alert('Article Successfully Edited!')
        $('#editArticleModal').modal('hide');
        this.loadArticle()
    }

    checkArticleSubmitter(posterName:string){
        if(this.userBlockService.getUserData().role == "Admin"){
            return true;
        }else if(posterName == this.userBlockService.getUserData().name){
            return true
        }else{
            return false
        }
    }
}