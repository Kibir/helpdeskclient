import { NgModule } from '@angular/core';
import { KnowledgeBaseComponent } from './knowledgeBase.component'
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CreateArticleComponent } from 'app/routes/knowledgeBase/create-article.component';
import { ArticleService } from 'app/core/data-services/article.service';
import { EmployeeService } from 'app/core/data-services/employee.service';
import { ArticleDetailComponent } from 'app/routes/knowledgeBase/article-detail.component';
import { EditArticleComponent } from 'app/routes/knowledgeBase/article-edit.component';
import { KnowledgeSearchDislayComponent } from 'app/routes/knowledgeBase/knowledge-results.component';
import { PagerService } from 'app/core/data-services/pager.service';
//import { EditorModule } from '@tinymce/tinymce-angular';

const routes: Routes = [
    { path: '', component:  KnowledgeBaseComponent },
    { path: 'detail', component: ArticleDetailComponent },
    { path: 'search', component: KnowledgeSearchDislayComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        //EditorModule
        
        
    ],
    declarations: [
        KnowledgeBaseComponent,
        CreateArticleComponent,
        ArticleDetailComponent,
        EditArticleComponent,
        KnowledgeSearchDislayComponent
    ],
    exports: [
        RouterModule
    ],providers:[
       ArticleService, EmployeeService, PagerService
    ]
})
export class KnowledgeModule { }