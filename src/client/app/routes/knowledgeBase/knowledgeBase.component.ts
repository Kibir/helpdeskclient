import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ArticleService } from 'app/core/data-services/article.service';
import { Article } from 'app/core/data-services/article';
import { EmployeeService } from 'app/core/data-services/employee.service';
import { Subscription } from 'rxjs/Subscription';
import { Employee } from 'app/core/data-services/employee';
import { Router } from '@angular/router';
import { PagerService } from 'app/core/data-services/pager.service';
declare var $:any

@Component({
    selector: 'knowledge-base',
    templateUrl: './knowledgeBase.component.html'
})

export class KnowledgeBaseComponent implements OnInit, OnDestroy{
   
    private articleSub:Subscription
    pageTitle:String = 'Knowledge Base'
    searchArticleForm:FormGroup
    private articleList:Article[]
    private tempEmp:Employee

    private ngUnsubscribe: Subject<any> = new Subject();

    pager: any = {};
    pagedItems: Article[];

   
    filterList:string[] = ['Filter','Everyone','My Team','Mine']
    isUserLoggedIn:boolean = false
    isUserAdmin: boolean = false
    private createClicked = true
    
    errorMessage:any
    private eName:string = ''

    constructor(public userBlockService:UserblockService, 
                private articleService:ArticleService, 
                private authService:AuthService,
                private employeeService:EmployeeService, 
                private fb:FormBuilder, 
                private _router:Router,
                private pagerService:PagerService){
        
    }
    ngOnInit() {
        this.searchArticleForm = this.fb.group({
            searchTitle:['']
        })
      this.loadData()
     
    }

    loadData(){
        this.articleSub = this.articleService.getArticles().subscribe(articles => {
            this.articleList = articles
            this.setPage(1)})
        
    }

    ngOnDestroy(){
        this.articleSub.unsubscribe()
     
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    summarizeData(data:string): string {
        if(data.length <= 116){
            return data
        }else{
            return data.substring(0,116).trim() + "..."
        }       
    }

    articleClick(article:Article){1
        this._router.navigate(['/knowledgeBase/detail',{id:article.id}])
    }

    newArticleCreated(){
        alert('New Article Successfully Created!')
        $('#createArticleModal').modal('hide');
        this.loadData()
    }

    newArticleEdited(){

    }

    evokeSearch(){
        let searchData:string = this.searchArticleForm.get('searchTitle').value
        this._router.navigate(['/knowledgeBase/search',{sd:searchData}])
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
    
        this.pager = this.pagerService.getPager(this.articleList.length, page);
        
        this.pagedItems = this.articleList.slice(this.pager.startIndex, this.pager.endIndex + 1);
       
    }

    
}