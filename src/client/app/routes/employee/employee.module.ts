import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import {EmployeeListComponent } from './employee-list.component'

import { EmployeeService } from 'app/core/data-services/employee.service';
import { CreateEmployeeComponent } from './create-employee.component';
import { EditEmployeeComponent } from 'app/routes/employee/employee-edit.component';
import { EmployeeDetailComponent } from 'app/routes/employee/employee-detail.component';



const routes: Routes = [
    { path: '', component: EmployeeListComponent },
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
      EmployeeListComponent,
      CreateEmployeeComponent, 
      EditEmployeeComponent,
      EmployeeDetailComponent
    ],
    exports: [
        RouterModule
    ],
    providers:[
        EmployeeService
    ]
})
export class EmployeeModule { }