import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'app/core/data-services/employee.service';
import { FormBuilder, Validators, FormArray, FormGroup, AbstractControl, ValidatorFn } from '@angular/forms';
import { EmployeeDetail } from 'app/core/data-services/employeeDetail';
import { ProfessionByEmployee } from 'app/core/data-services/professionByEmployee';


function emailMatcher(c: AbstractControl): {[key: string]: boolean} | null {
    let emailControl = c.get('email');
    let confirmControl = c.get('confirmEmail');

    if (emailControl.pristine || confirmControl.pristine) {
      return null;
    }

    if (emailControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
 }

 function passwordMatcher(c: AbstractControl): {[key: string]: boolean} | null {
    let password = c.get('password');
    let re_enteredPassword = c.get('reenteredPassword');

    if (password.pristine || re_enteredPassword.pristine) {
      return null;
    }

    if (password.value === re_enteredPassword.value) {
        return null;
    }
    return { 'match': true };
 }

 function yearsInActiveRange(min: number, max: number): ValidatorFn {
    return  (c: AbstractControl): {[key: string]: boolean} | null => {
        if (c.value !== undefined && (isNaN(c.value) || c.value < min || c.value > max)) {
            return { 'range': true };
        };
        return null;
    };
}

@Component({
    selector: 'create-employee',
    templateUrl: './create-employee.component.html',
})

export class CreateEmployeeComponent implements OnInit{
   
    @Output() employeeCreated = new EventEmitter()
    constructor(private fb: FormBuilder, private employeeService:EmployeeService){
        
    }
    createEmployeeForm: FormGroup;
    
    emailMessage: string;
    private departmentMap = new Map<number, string>(); 
    

    get addresses(): FormArray{
        return <FormArray>this.createEmployeeForm.get('addresses');
    }

    get degrees(): FormArray{
        return <FormArray>this.createEmployeeForm.get('degrees');
    }

    get certificates(): FormArray{
        return <FormArray>this.createEmployeeForm.get('certificates');
    }

    private validationMessages = {
        required: 'Please enter your email address.',
        pattern: 'Please enter a valid email address.'
    };


    ngOnInit(): void {
        this.departmentMap.set(1,"IT Development")
        this.departmentMap.set(2,"Networking")
        this.departmentMap.set(3,"Finance")
        this.departmentMap.set(4,"Human Resource")
       
        this.createEmployeeForm = this.fb.group({
            firstName: ['', [Validators.required, Validators.minLength(3)]],
            lastName: ['', [Validators.required, Validators.maxLength(50)]],
            emailGroup: this.fb.group({
                email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+')]],
                confirmEmail: ['', Validators.required],
            }, {validator: emailMatcher}),
            userRole:['', [Validators.required, Validators.maxLength(20)]],
            passwordGroup: this.fb.group({
                password: ['', [Validators.required]],
                reenteredPassword: ['', Validators.required],
            }, {validator: passwordMatcher}),
            departmentName:'',
            supervisor:['', [Validators.required, Validators.maxLength(50)]],
            profession:['', [Validators.required, Validators.maxLength(30)]],
            yearsInActive:['',yearsInActiveRange(1,80)],
            levelofEdu:['', [Validators.required, Validators.maxLength(50)]],
            nrc:['', [Validators.required, Validators.maxLength(30)]],
            degrees: this.fb.array([this.buildDegrees()]),
            certificates: this.fb.array([this.buildCertificates()])
          
        });

        const emailControl = this.createEmployeeForm.get('emailGroup.email');
        emailControl.valueChanges.debounceTime(1000).subscribe(value =>
            this.setMessage(emailControl));
    }

    getDepartments(){
        return Array.from(this.departmentMap.values())
    }

    addCertificate(): void {
        this.certificates.push(this.buildCertificates());
    }

    

    addDegree(): void {
        this.degrees.push(this.buildDegrees());
    }

    buildDegrees(): FormGroup {
        return this.fb.group({
              name:''
        });
    }

    buildCertificates(): FormGroup {
        return this.fb.group({
                name: '',
        });
    }

    createEmployee(): void {
        let employee: Employee = new Employee();
        let employeeDetail: EmployeeDetail = new EmployeeDetail();
        let professionByEmployee: ProfessionByEmployee = new ProfessionByEmployee();
        let dpId:number = 0
        employee.firstName = this.createEmployeeForm.get('firstName').value
        employee.lastName = this.createEmployeeForm.get('lastName').value
        employee.email = this.createEmployeeForm.get('emailGroup.email').value
        employee.userRole = this.createEmployeeForm.get('userRole').value
        employee.password = this.createEmployeeForm.get('passwordGroup.password').value
        employee.employeeDetail = employeeDetail
        // employee.professionByEmployee = professionByEmployee
       
        for(let i = 0;i < this.departmentMap.size-1;i++){
            if(this.departmentMap.get(i) == this.createEmployeeForm.get('departmentName').value){
                dpId = i
            }
        }

        if(this.createEmployeeForm.get('departmentName').value == 0 || this.createEmployeeForm.get('departmentName').value == undefined){
                dpId = 1
        }
        employee.employeeDetail.departmentId = dpId
        employee.employeeDetail.supervisor = this.createEmployeeForm.get('supervisor').value
        employee.employeeDetail.profession = this.createEmployeeForm.get('profession').value
        employee.employeeDetail.yearsInActive = this.createEmployeeForm.get('yearsInActive').value
        employee.employeeDetail.levelofEducation = this.createEmployeeForm.get('levelofEdu').value
        employee.employeeDetail.NRC = this.createEmployeeForm.get('nrc').value
        employee.degrees = this.createEmployeeForm.get('degrees').value
        employee.certificates = this.createEmployeeForm.get('certificates').value
       
        this.employeeService.saveEmployee(employee).subscribe(() => this.afterCreatingEmployee())
      
    }

    loadEmployeeData(){

    }

    setMessage(c: AbstractControl): void {
        this.emailMessage = '';
        if ((c.touched || c.dirty) && c.errors) {
            this.emailMessage = Object.keys(c.errors).map(key =>
                this.validationMessages[key]).join(' ');
        }
    }
    
    afterCreatingEmployee(){
      this.employeeCreated.emit('New Employee Created!')
        alert('Employee Created!')
    }

    formReset(){
        this.createEmployeeForm.reset()
       this.degrees.reset()
       this.certificates.reset()
    }
}