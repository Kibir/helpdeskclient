import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms/';
import { Employee } from 'app/core/data-services/employee';
import { EmployeeService } from 'app/core/data-services/employee.service';

@Component({
    selector: 'employee-detail',
    templateUrl: './employee-detail.component.html'
})

export class EmployeeDetailComponent implements OnInit {
    imageUrl: string
    @Input() employee: Employee
    @Input() employeeDeleted: boolean

    isUserLoggedIn: boolean = false
    private inputEmployee: Employee;
    private initialized: boolean = false
    errorMessage: any

    constructor(private userBlockService: UserblockService, private employeeService: EmployeeService, private fb: FormBuilder) {

    }
    ngOnInit() {
        if (this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined) {
            this.isUserLoggedIn = true
        }
        this.initialized = true
    }
    ngOnChanges() {
        if (this.initialized) {
            this.inputEmployee = this.employee
        }
    }

    getDepartmentName(id:number):string{
        if(id == 1){
            return "IT Development"
        }else if(id == 2){
            return "Networking"
        }else if(id == 3){
            return "Finance"
        }else if(id == 4){
            return "Human Resource"
        }else{
            return "Unknown"
        }
    }
}