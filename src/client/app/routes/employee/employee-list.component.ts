import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'app/core/data-services/employee.service';

declare var $:any;
@Component({
    selector: 'employee-list',
    templateUrl: './employee-list.component.html',
    styles: [`
   `]
})

export class EmployeeListComponent implements OnInit{
    pageTitle:String = 'Employees'
    employeeUser:Employee
    employees:Employee[]
    private isUserLoggedIn:boolean = false
    private isUserAdmin: boolean = false
    errorMessage:any
    employeeDeleted: boolean = false
    selectedEmployee: Employee


    constructor(public userBlockService:UserblockService,private route:ActivatedRoute, private authService:AuthService, private employeeService:EmployeeService){
        
    }

    ngOnInit(): void {
        
       if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
           this.isUserLoggedIn = true
           if(this.userBlockService.getUserData().role == "Admin"){
               this.isUserAdmin = true
           }
       }

        this.loadEmployeeData()
    }

    loadEmployeeData(){
        this.employeeService.getEmployeeData().map(employees => employees.filter(e => e.id != 0)).subscribe(employee => this.employees = employee)
    }

    deleteEmployee(id:number){
        this.employeeService.deleteEmployee(id).subscribe(
            () => this.afterDeletingEmployee(id),
            (error: any) => this.errorMessage = <any>error
        );
        this.loadEmployeeData()
    }

    afterDeletingEmployee(id:number){
        if (id == this.selectedEmployee.id) {
            this.employeeDeleted = true
            
        } else {
            this.employeeDeleted = false
        }
        alert('Successfully Deleted!')
    }

    onEmployeeCreated(event:any){
        $('#createEmployeeModal').modal('hide');
        alert('Successfully Created!')
        this.loadEmployeeData()
    }

    onEmployeeEdited(event:any){
        $('#editEmployeeModal').modal('hide');
        alert('Successfully Edited!')
        this.loadEmployeeData()
    }

    setSelectedEmployee(employee:Employee){
        this.selectedEmployee = employee
        this.employeeDeleted = false
    }
}