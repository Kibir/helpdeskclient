import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl, ValidatorFn } from '@angular/forms';
import { EmployeeService } from 'app/core/data-services/employee.service';
import { Employee } from 'app/core/data-services/employee';
import { ProfessionByEmployee } from 'app/core/data-services/professionByEmployee';
import { EmployeeDetail } from 'app/core/data-services/employeeDetail';

function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    let emailControl = c.get('email');
    let confirmControl = c.get('confirmEmail');

    if (emailControl.pristine || confirmControl.pristine) {
        return null;
    }

    if (emailControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
}

function passwordMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    let password = c.get('password');
    let re_enteredPassword = c.get('reenteredPassword');

    if (password.pristine || re_enteredPassword.pristine) {
        return null;
    }

    if (password.value === re_enteredPassword.value) {
        return null;
    }
    return { 'match': true };
}

function yearsInActiveRange(min: number, max: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value !== undefined && (isNaN(c.value) || c.value < min || c.value > max)) {
            return { 'range': true };
        };
        return null;
    };
}

@Component({
    selector: 'edit-employee',
    templateUrl: './employee-edit.component.html'
})

export class EditEmployeeComponent implements OnInit {
    editEmployeeForm: FormGroup

    private isUserLoggedIn: boolean = false
    private isUserAdmin: boolean = false
    private departmentMap = new Map<number, string>();
    private initialized: boolean = false
    private employeeToBeEdited: Employee = new Employee()
    private certificatesArray: FormArray;
    private degreesArray :FormArray;
    errorMessage: any

    @Input() selectedEmployee: Employee

    emailMessage: string;


    @Output() employeeEdited: EventEmitter<string> = new EventEmitter<string>();

    private emailValidationMessages = {
        required: 'Please enter your email address.',
        pattern: 'Please enter a valid email address.'
    };

    constructor(public userBlockService: UserblockService, private fb: FormBuilder, private employeeService: EmployeeService) {

    }

    ngOnInit() {
        this.departmentMap.set(1, "IT Development")
        this.departmentMap.set(2, "Networking")
        this.departmentMap.set(3, "Finance")
        this.departmentMap.set(4, "Human Resource")

        this.editEmployeeForm = this.fb.group({
            firstName: ['', [Validators.required, Validators.minLength(3)]],
            lastName: ['', [Validators.required, Validators.maxLength(50)]],
            emailGroup: this.fb.group({
                email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+')]],
                confirmEmail: ['', Validators.required],
            }, { validator: emailMatcher }),
            userRole: ['', [Validators.required, Validators.maxLength(20)]],
            passwordGroup: this.fb.group({
                password: ['', [Validators.required]],
                reenteredPassword: ['', Validators.required],
            }, { validator: passwordMatcher }),
            departmentName: '',
            supervisor: ['', [Validators.required, Validators.maxLength(50)]],
            profession: ['', [Validators.required, Validators.maxLength(30)]],
            yearsInActive: ['', yearsInActiveRange(1, 80)],
            levelofEdu: ['', [Validators.required, Validators.maxLength(50)]],
            nrc: ['', [Validators.required, Validators.maxLength(30)]],
            degrees: this.fb.array([]),
            certificates: this.fb.array([])

        });

        const emailControl = this.editEmployeeForm.get('emailGroup.email');
        emailControl.valueChanges.debounceTime(1000).subscribe(value =>
            this.setMessage(emailControl));

        this.initialized = true
        this. certificatesArray = <FormArray>this.editEmployeeForm.controls['certificates'];
         this.degreesArray = <FormArray>this.editEmployeeForm.controls['degrees'];
    }

    setMessage(c: AbstractControl): void {
        this.emailMessage = '';
        if ((c.touched || c.dirty) && c.errors) {
            this.emailMessage = Object.keys(c.errors).map(key =>
                this.emailValidationMessages[key]).join(' ');
        }
    }

    getDepartments() {
        return Array.from(this.departmentMap.values())
    }

    addCertificate(): void {
        this.certificates.push(this.buildCertificates());
    }

    addDegree(): void {
        this.degrees.push(this.buildDegrees());
    }

    get degrees(): FormArray {
        return <FormArray>this.editEmployeeForm.get('degrees');
    }

    get certificates(): FormArray {
        return <FormArray>this.editEmployeeForm.get('certificates');
    }

    buildDegrees(): FormGroup {
        return this.fb.group({
            name: ''
        });
    }

    buildCertificates(): FormGroup {
        return this.fb.group({
            name: '',
        });
    }

    ngOnChanges() {
        if (this.initialized) {
            this.fillFormData()
        }

    }

    fillFormData() {
        if (this.initialized) {

            this.employeeToBeEdited = this.selectedEmployee
            
            this.editEmployeeForm.patchValue({
                firstName: [this.employeeToBeEdited.firstName],
                lastName: [this.employeeToBeEdited.lastName],
                emailGroup: this.fb.group({
                    email: [this.employeeToBeEdited.email],
                    confirmEmail: [this.employeeToBeEdited.email],
                }),
                userRole: [this.employeeToBeEdited.userRole],
                passwordGroup: this.fb.group({
                    password: [this.employeeToBeEdited.password],
                    reenteredPassword: [this.employeeToBeEdited.password],
                }),
                departmentName: [this.departmentMap.get(this.employeeToBeEdited.employeeDetail.departmentId)],
                supervisor: [this.employeeToBeEdited.employeeDetail.supervisor],
                profession: [this.employeeToBeEdited.employeeDetail.profession],
                yearsInActive: [this.employeeToBeEdited.employeeDetail.yearsInActive],
                levelofEdu: [this.employeeToBeEdited.employeeDetail.levelofEducation],
                nrc: [this.employeeToBeEdited.employeeDetail.NRC],
            });

            (<FormGroup>this.editEmployeeForm.controls['emailGroup']).controls['email'].patchValue(this.employeeToBeEdited.email);
            (<FormGroup>this.editEmployeeForm.controls['emailGroup']).controls['confirmEmail'].patchValue(this.employeeToBeEdited.email);
            (<FormGroup>this.editEmployeeForm.controls['passwordGroup']).controls['password'].patchValue(this.employeeToBeEdited.password);
            (<FormGroup>this.editEmployeeForm.controls['passwordGroup']).controls['reenteredPassword'].patchValue(this.employeeToBeEdited.password);
           


            this.employeeToBeEdited.certificates.forEach(certificate => {
                const fb = this.buildCertificates();
                fb.patchValue(certificate);
                this.certificatesArray.push(fb);
            });

            this.employeeToBeEdited.degrees.forEach(degrees => {
                const fb = this.buildDegrees();
                fb.patchValue(degrees);
                this.degreesArray.push(fb);
            });

        }
    }
    editEmployee(): void {

        if (this.editEmployeeForm.valid) {
            let dpId: number = 0
            this.employeeToBeEdited.firstName = this.editEmployeeForm.get('firstName').value
            this.employeeToBeEdited.lastName = this.editEmployeeForm.get('lastName').value
            this.employeeToBeEdited.email = this.editEmployeeForm.get('emailGroup.email').value
            this.employeeToBeEdited.userRole = this.editEmployeeForm.get('userRole').value
            this.employeeToBeEdited.password = this.editEmployeeForm.get('passwordGroup.password').value

            for (let i = 0; i < this.departmentMap.size - 1; i++) {
                if (this.departmentMap.get(i) == this.editEmployeeForm.get('departmentName').value) {
                    dpId = i
                }
            }

            if (this.editEmployeeForm.get('departmentName').value == 0 || this.editEmployeeForm.get('departmentName').value == undefined) {
                dpId = 1
            }
            this.employeeToBeEdited.employeeDetail.departmentId = dpId
            this.employeeToBeEdited.employeeDetail.supervisor = this.editEmployeeForm.get('supervisor').value
            this.employeeToBeEdited.employeeDetail.profession = this.editEmployeeForm.get('profession').value
            this.employeeToBeEdited.employeeDetail.yearsInActive = this.editEmployeeForm.get('yearsInActive').value
            this.employeeToBeEdited.employeeDetail.levelofEducation = this.editEmployeeForm.get('levelofEdu').value
            this.employeeToBeEdited.employeeDetail.NRC = this.editEmployeeForm.get('nrc').value
            this.employeeToBeEdited.degrees = this.editEmployeeForm.get('degrees').value
            this.employeeToBeEdited.certificates = this.editEmployeeForm.get('certificates').value

            this.employeeService.updatingEmployee(this.employeeToBeEdited).subscribe(() => this.afterEditingEmployee())
        }else{
            alert('The form isn\'t valid yet! Check the entered values and try again!')
        }
    }

    afterEditingEmployee() {
        this.employeeEdited.emit('Employee Edited!')
    }

    formReset() {
        this.editEmployeeForm.reset()
        while (this.degrees.length) {
            this.degrees.removeAt(0);
        }
        while (this.certificates.length) {
            this.certificates.removeAt(0);
        }
    }
}