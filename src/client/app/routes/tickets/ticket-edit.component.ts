import { Component, OnInit, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { FormGroup, FormBuilder, AbstractControl, ValidatorFn, Validators } from '@angular/forms';
import { EmployeeService } from 'app/core/data-services/employee.service';
import { Employee } from 'app/core/data-services/employee';



function priorityValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select a priority') {
            return { 'notSelected': true };
        };
        return null;
    };
}


function categoryValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select a category') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function dayValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'DD') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function monthValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'MM') {
            return { 'notSelected': true };
        };
        return null;
    };
}


function yearValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'YYYY') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function progressValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value > 100 || c.value < 0) {
            return { 'notInRange': true };
        };
        return null;
    };
}

@Component({
    selector: 'ticket-edit',
    templateUrl: './ticket-edit.component.html'
})

export class TicketEditComponent implements OnInit {
    editTicketForm: FormGroup
    @Input() selectedTicket: Ticket
    @Output() ticketEdited: EventEmitter<string> =
        new EventEmitter<string>();
    employees: Employee[]

    private initialized: boolean = false
    private previousAssignee: string = null
    private ticketToBeEdited: Ticket = new Ticket()

    private dayOptions: string[] = ["DD", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    private dueDayOptions: string[] = this.dayOptions
    private monthOptions: string[] = ["MM", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    private yearOptions: string[] = ["YYYY", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029"]
    private dueDate: Date
    private categoryList: string[] = ["Select a category", "Email", "Hardware", "Network", "Maintenance", "Printer", "Software", "Other"]
    private priorityList: string[] = ["Select a priority", "High", "Medium", "Low"]
    private employeeMap = new Map<number, string>();
    errorMessage: any;
    private dueDateInvalid: boolean = false


    constructor(private fb: FormBuilder, private ticketService: TicketService, private employeeService: EmployeeService) {
    }
    ngOnInit() {
        this.employeeMap.set(0, 'Choose Employee/Assignee')
        this.employeeService.getEmployeeData()
            .subscribe(emp => this.employees = emp,
            (err) => console.error(err),
            () => {
                for (let i = 0; i < this.employees.length; i++) {
                    this.employeeMap.set(this.employees[i].id, this.employees[i].firstName)
                }
            })
        this.editTicketForm = this.fb.group({
            creator: [''],
            progress: ['', [progressValidator()]],
            summary: ['', [Validators.required, Validators.maxLength(50), Validators.minLength(3)]],
            description: ['', [Validators.required, Validators.maxLength(200), Validators.minLength(50)]],
            workSheetBody: ['', [Validators.required, Validators.minLength(50)]],
            assignee: [''],
            dueYear: ['YYYY', [yearValidator()]],
            dueMonth: ['MM', [monthValidator()]],
            dueDay: ['DD', [dayValidator()]],
            priority: ['Select a priority', [priorityValidator()]],
            category: ['Select a category', [categoryValidator()]],
        });

        this.initialized = true
    }

    ngOnChanges() {

        if (this.initialized) {
            this.ticketToBeEdited = this.selectedTicket
            this.ticketService.getTicket(this.selectedTicket.id)
                .subscribe(ticket => this.previousAssignee = ticket.assignee)
            this.setFormData()

        }
    }

    getEmployees() {
        return Array.from(this.employeeMap.values())
    }

    setDates() {
        this.dueDate = new Date(this.editTicketForm.get('dueYear').value,
            this.editTicketForm.get('dueMonth').value,
            this.editTicketForm.get('dueDay').value)
    }

    editTicket(event: any) {

        if (this.editTicketForm.valid) {

            this.setDates()
            let id: number = this.ticketToBeEdited.id
            let empId: number = 0
            let today: Date = new Date()

            if (this.dueDate >= today) {
                if (this.editTicketForm.get('dueYear').value != undefined && this.editTicketForm.get('dueYear').value != "" &&
                    this.editTicketForm.get('dueMonth').value != undefined && this.editTicketForm.get('dueMonth').value != "" &&
                    this.editTicketForm.get('dueDay').value != undefined && this.editTicketForm.get('dueDay').value != "") {
                    this.ticketToBeEdited.dueDate = this.dueDate.toJSON()
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }

                if (this.editTicketForm.get('summary').value != undefined && this.editTicketForm.get('summary').value != "") {
                    this.ticketToBeEdited.summary = this.editTicketForm.get('summary').value
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }
                if (this.editTicketForm.get('description').value != undefined && this.editTicketForm.get('description').value != "") {
                    this.ticketToBeEdited.description = this.editTicketForm.get('description').value
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }
                if (this.editTicketForm.get('workSheetBody').value != undefined && this.editTicketForm.get('workSheetBody').value != "") {
                    this.ticketToBeEdited.workSheetBody = this.editTicketForm.get('workSheetBody').value
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }
                if (this.editTicketForm.get('assignee').value != undefined && this.editTicketForm.get('assignee').value != "") {
                    if (this.editTicketForm.get('assignee').value != this.previousAssignee) {
                        this.ticketToBeEdited.assignee = this.editTicketForm.get('assignee').value
                        for (let i = 0; i < this.employees.length; i++) {

                            if (this.employees[i].firstName == this.editTicketForm.get('assignee').value) {

                                empId = this.employees[i].id


                                break;
                            }
                        }
                        this.ticketToBeEdited.assigneeEID = empId
                        this.ticketToBeEdited.acceptStatus = false
                        this.ticketToBeEdited.lastUpdated = today.toJSON();

                    }
                } else if (this.editTicketForm.get('assignee').value != undefined || this.editTicketForm.get('assignee').value != ""
                    || this.editTicketForm.get('assignee').value != "Choose Employee/Assignee") {
                    this.ticketToBeEdited.assigneeEID = null
                    this.ticketToBeEdited.assignee = null
                    this.ticketToBeEdited.acceptStatus = false
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }
                if (this.editTicketForm.get('priority').value != undefined && this.editTicketForm.get('priority').value != "") {
                    this.ticketToBeEdited.priority = this.editTicketForm.get('priority').value
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }
                if (this.editTicketForm.get('category').value != undefined && this.editTicketForm.get('category').value != "") {
                    this.ticketToBeEdited.category = this.editTicketForm.get('category').value
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }
                if (this.editTicketForm.get('progress').value != undefined && this.editTicketForm.get('progress').value != "") {
                    this.ticketToBeEdited.progress = this.editTicketForm.get('progress').value
                    this.ticketToBeEdited.lastUpdated = today.toJSON();
                }
                this.ticketService.updatingTicket(this.ticketToBeEdited).subscribe(
                    () => this.afterEditingTicket(),
                    (error: any) => this.errorMessage = error)
            }else{
                this.dueDateInvalid = true
            }
        }
    }

    afterEditingTicket() {
        this.ticketEdited.emit("Ticket Successfully Edited!")
        alert("Ticket Successfully Edited!")
        this.closeModal()

    }

    closeModal() {

        this.setFormData()
    }

    setFormData() {
        this.editTicketForm.reset()
        this.dueDateInvalid = false
        if (this.ticketToBeEdited.creator) {
            this.editTicketForm.patchValue({
                creator: this.ticketToBeEdited.creator,
                progress: this.ticketToBeEdited.progress,
                summary: this.ticketToBeEdited.summary,
                description: this.ticketToBeEdited.description,
                assignee: this.ticketToBeEdited.assignee,
                workSheetBody: this.ticketToBeEdited.workSheetBody,
                dueYear: new Date(this.ticketToBeEdited.dueDate).getFullYear(),
                dueMonth: new Date(this.ticketToBeEdited.dueDate).getMonth(),
                dueDay: new Date(this.ticketToBeEdited.dueDate).getDay(),
                priority: this.ticketToBeEdited.priority,
                category: this.ticketToBeEdited.category,
            });
        } else if (this.ticketToBeEdited.creatorCustomerName) {
            this.editTicketForm.patchValue({
                creator: this.ticketToBeEdited.creatorCustomerName,
                progress: this.ticketToBeEdited.progress,
                summary: this.ticketToBeEdited.summary,
                description: this.ticketToBeEdited.description,
                assignee: this.ticketToBeEdited.assignee,
                workSheetBody: this.ticketToBeEdited.workSheetBody,
                dueYear: new Date(this.ticketToBeEdited.dueDate).getFullYear(),
                dueMonth: new Date(this.ticketToBeEdited.dueDate).getMonth(),
                dueDay: new Date(this.ticketToBeEdited.dueDate).getDay(),
                priority: this.ticketToBeEdited.priority,
                category: this.ticketToBeEdited.category,
            });
        }
    }

    dueDayChange(input: number, event: any) {
        var previousData = this.editTicketForm.get('dueDay').value
        if (input == 2) {
            this.dueDayOptions = this.dayOptions.slice(0, 29)
            this.editTicketForm.get('dueDay').updateValueAndValidity();

            if (previousData == 30 || previousData == 31 || previousData == "") {

                this.editTicketForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.editTicketForm.get('dueDay').setValue(previousData);
            }
        } else if (input == 9 || input == 4 || input == 6 || input == 11) {
            this.dueDayOptions = this.dayOptions.slice(0, 31)
            this.editTicketForm.get('dueDay').updateValueAndValidity();

            if (previousData == 31 || previousData == "") {
                this.editTicketForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.editTicketForm.get('dueDay').setValue(previousData);
            }
        } else {
            this.dueDayOptions = this.dayOptions.slice(0, 32)
            this.editTicketForm.get('dueDay').updateValueAndValidity();
            if (previousData == "") {
                this.editTicketForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.editTicketForm.get('dueDay').setValue(previousData);
            }
        }
    }

}