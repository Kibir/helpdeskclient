import { Component, OnInit } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { DatePipe } from '@angular/common';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from 'app/core/data-services/pager.service';
import { TableExport } from 'tableexport'
declare var $:any;

@Component({
    selector: 'closedTicket-list',
    templateUrl: './closedTicket-list.component.html',
    styles: [`
   `]
})

export class ClosedTicketListComponent implements OnInit{
    pageTitle:String = 'Closed Tickets'
    employeeUser:Employee
    
    listFilter:String = ''
    tickets:Ticket[]
    selectedTicket:Ticket
    ticketDeleted:boolean = false
    initialLength:number
    errorMessage:any

     // pager object
     pager: any = {};
     
        // paged items
    pagedItems: Ticket[];

    private isUserLoggedIn:boolean = false
    private isUserPrivileged:boolean = false

    constructor(public userBlockService:UserblockService,
                private route:ActivatedRoute, 
                public authService:AuthService, 
                private ticketService:TicketService,
                private pagerService:PagerService ){ 
    }
    ngOnInit(): void {
        
       if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
           this.isUserLoggedIn = true
           if(this.userBlockService.getUserData().role == "Admin" ||
           this.userBlockService.getUserData().role == "Supervisor" ||
           this.userBlockService.getUserData().role == "Manager"){
            this.isUserPrivileged = true
        }
       }
        this.refreshList()
        this.authService.getUser(this.userBlockService.getUserData().id)
        .subscribe(user => this.employeeUser = user) 
        
    }
 
    refreshList(){
        this.ticketService.getTickets().map(tickets => tickets.filter(ticket => ticket.status === "Closed"))
        .subscribe(tickets => {
            this.tickets = tickets
            this.setPage(1);
        },
                   error => this.errorMessage = <any>error);
        
        
    }

    reopenTicket(){
       this.refreshList()
                   this.ticketDeleted = true
        $('#reopenModal').modal('hide');
    }

    selectTicketToReopen(ticket:Ticket){
        this.selectedTicket = ticket
        this.ticketDeleted = false
    }

    setSelectedTicket(ticket:Ticket){
        this.selectedTicket = ticket
        this.ticketDeleted = false
        console.log(this.selectedTicket.id)
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.pagerService.getPager(this.tickets.length, page);
 
        this.pagedItems = this.tickets.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    export() {
        var instance = new TableExport(document.getElementById('closedTicketList'),
            {
                headers: true,
                footers: true,
                formats: ['csv'],
                filename: 'ClosedTicketList',
                bootstrap: false,
                exportButtons: false,
                position: 'bottom',
                ignoreRows: null,
                ignoreCols: null,
                trimWhitespace: true
            });
        var exportDataCSV = instance.getExportData()['closedTicketList']['csv'];
        instance.export2file(exportDataCSV.data, exportDataCSV.mimeType, exportDataCSV.filename, exportDataCSV.fileExtension);
        alert('Closed Ticket list successfully exported!')
    }
}