import { Component, OnInit, OnChanges, AfterContentInit } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import { PagerService } from 'app/core/data-services/pager.service';

declare var $:any;

@Component({
    selector: 'myTicket-list',
    templateUrl: './myTickets.component.html',
    styles: [`
   `]
})

export class MyTicketsComponent implements OnInit{
    pageTitle:String = 'My Tickets'
    employeeUser:Employee
    isUserLoggedIn:boolean = false
    listFilter:String = ''
    tickets:Ticket[]
    selectedTicket:Ticket
    errorMessage:any

     pager: any = {};
    pagedItems: Ticket[];

    constructor(public userBlockService:UserblockService,
                private route:ActivatedRoute,
                public authService:AuthService, 
                private ticketService:TicketService,
                private pagerService:PagerService)
    {
    
    }
    ngOnInit() {
        
       if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
           this.isUserLoggedIn = true
       }
    
        this.authService.getUser(this.userBlockService.getUserData().id)
        .subscribe(user => this.employeeUser = user) 

        this.refreshList()

        
    }

    ngOnChanges(){
        this.refreshList()
    }

    refreshList(){
        this.ticketService.getTickets()
        .map(tickets => tickets.filter(ticket => ticket.assignee === this.userBlockService.getUserData().name))
        .subscribe(tickets => {
            this.tickets = tickets
            this.setPage(1);
        }, error => this.errorMessage = <any>error)   
    }

    setSelectedTicket(ticket:Ticket){
        this.selectedTicket = ticket
    }

    validateTicketAssignee(ticket:Ticket){
       return ticket.assignee == this.userBlockService.getUserData().name
    }

    acceptTicket(ticket:Ticket){
        if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
             
          ticket.assignee = this.employeeUser.firstName
          ticket.assigneeEID = this.employeeUser.id
          ticket.acceptStatus = true
        }
        this.ticketService.updatingTicket(ticket).subscribe(() => this.afterAcceptingTask())
    }

    afterAcceptingTask(){
        alert('Ticket Accepted!')
        this.refreshList()
    }
    onTicketEdit(){
        this.refreshList()
        $('#editModal').modal('hide');
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        // get pager object from service
        this.pager = this.pagerService.getPager(this.tickets.length, page);
 
        // get current page of items
        this.pagedItems = this.tickets.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}