import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { FormGroup,FormBuilder } from '@angular/forms';

@Component({
    selector: 'close-ticket',
    templateUrl: './closeTicket.component.html'
})

export class CloseTicketComponent implements OnInit{
    closeTicketForm:FormGroup
    @Input() selectedTicket:Ticket
    @Output() ticketClosed: EventEmitter<string> =
    new EventEmitter<string>();
    private initialized:boolean = false
    errorMessage:any;
   
    constructor(private fb:FormBuilder, private ticketService:TicketService){

    }
    ngOnInit(){
        this.closeTicketForm = this.fb.group({
            ticketId:[''],
            reasonForClosing:['']
        });
        this.initialized = true       
    }

    ngOnChanges(){
        if(this.initialized){
            this.fillFormData()
        }
    }

    fillFormData(){
        this.closeTicketForm = this.fb.group({
            ticketId:[this.selectedTicket.id],
            reasonForClosing:['']
        });
    }

    closeTicket(event: any){
        
        let id:number = this.selectedTicket.id
        this.selectedTicket.status = "Closed"
        this.selectedTicket.reasonForClosing = this.closeTicketForm.get('reasonForClosing').value
        this.ticketService.updatingTicket(this.selectedTicket).subscribe(
            () => this.afterClosingTicket(),
            (error:any) => this.errorMessage = error)
    }

    afterClosingTicket() {
        this.ticketClosed.emit("Ticket Successfully Closed!")
        this.closeTicketForm.setValue({
            ticketId:[''],
            reasonForClosing:['']
        });
        
    }

   

}