import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketReplyService } from 'app/core/data-services/ticket-reply.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TicketReply } from 'app/core/data-services/ticketreply';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { EmployeeService } from 'app/core/data-services/employee.service';
@Component({
    selector: 'ticketReply-Box',
    templateUrl: './ticketReply-Box.component.html',
})

export class TicketReplyBoxComponent implements OnInit {
    @Input() inputTicket: Ticket
    @Output() newReplySubmitted = new EventEmitter()
    responseForm: FormGroup
    isUserLoggedIn: boolean = false
    isUserAdmin: boolean = false
    editResponse: boolean = false
    replies: TicketReply[]
    private ticket: Ticket
    private ticketReply: TicketReply = new TicketReply()
    private replyToEdit: TicketReply = new TicketReply()

    constructor(private fb: FormBuilder, private employeeService: EmployeeService, private ticketReplyService: TicketReplyService, private userBlockService: UserblockService) {
    }

    ngOnInit() {
        this.responseForm = this.fb.group({
            responseType: ['Type'],
            responseBody: ['']
        })
        if (this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined) {
            this.isUserLoggedIn = true
            if (this.userBlockService.getUserData().role == 'Admin') {
                this.isUserAdmin = true
            }
        }
    }

    validateCommentPoster(posterName: string) {
        return this.userBlockService.getUserData().name === posterName
    }

    ngOnChanges() {
        if (this.inputTicket) {
            this.ticket = this.inputTicket
            this.updateOrRefreshReplies()
        }
    }

    updateOrRefreshReplies() {
        if (this.isUserAdmin) {
            this.ticketReplyService.getTicketReplies().map(tickets => tickets.filter(ticketReply => ticketReply.ticketId === this.ticket.id))
                .subscribe(ticketReplies => this.replies = ticketReplies)
        }
        else if (this.userBlockService.getUserData().name == this.ticket.assignee) {
            this.ticketReplyService.getTicketReplies().map(tickets => tickets.filter(ticketReply => ticketReply.ticketId === this.ticket.id))
            .map(tickets => tickets.filter(ticketReply => ticketReply.postedUserName === this.userBlockService.getUserData().name || ticketReply.responseType === 'Public'))
            .subscribe(ticketReplies => this.replies = ticketReplies)
        } else if (!this.isUserAdmin && this.userBlockService.getUserData().name != this.ticket.assignee) {
            this.ticketReplyService.getTicketReplies().map(tickets => tickets.filter(ticketReply => ticketReply.ticketId === this.ticket.id))
                .map(tickets => tickets.filter(ticketReply => ticketReply.responseType === 'Public'))
                .subscribe(ticketReplies => this.replies = ticketReplies)
        }

    }

    saveResponse() {
        if (this.isUserLoggedIn) {
            this.ticketReply.postedUserID = this.userBlockService.getUserData().id
            this.ticketReply.postedUserName = this.userBlockService.getUserData().name
            this.ticketReply.postedDateTime = new Date().toDateString()
            this.ticketReply.responseType = this.responseForm.get('responseType').value
            this.ticketReply.responseBody = this.responseForm.get('responseBody').value
            this.ticketReply.ticketId = this.ticket.id
            this.ticketReplyService.saveTicketReply(this.ticketReply).subscribe(() => this.afterSubmittingNewReply())
        }
        else {
            alert('You should be logged in to post a reply!')
        }
    }

    afterSubmittingNewReply() {

        this.newReplySubmitted.emit('')
        alert("New Reply Subbmited!")
        this.updateOrRefreshReplies()
    }

    afterUpdatingReply() {

        this.newReplySubmitted.emit('')
        alert("Reply Updated!")
        this.updateOrRefreshReplies()
    }

    afterDeletingReply() {
        alert("Reply Deleted!")
        this.updateOrRefreshReplies()
    }


    editButtonClicked(ticketReply: TicketReply) {
        window.location.hash = ""
        alert("Edit button clicked!")
        window.location.hash = "#responseForm"

        this.editResponse = true
        this.responseForm.setValue({
            responseType: [ticketReply.responseType],
            responseBody: [ticketReply.responseBody]
        })
        this.replyToEdit = ticketReply

    }

    editTicketReply() {
        this.replyToEdit.responseBody = this.responseForm.get('responseBody').value
        this.replyToEdit.responseType = this.responseForm.get('responseType').value
        this.ticketReplyService.updatingTicket(this.replyToEdit).subscribe(() => this.afterUpdatingReply())
        this.editResponse = false
        this.responseForm.setValue({
            responseType: ['Type'],
            responseBody: ['']
        })
    }

    deleteButtonClicked(ticketReply: TicketReply) {
        this.editResponse = false
        this.responseForm.setValue({
            responseType: ['Type'],
            responseBody: ['']
        })
        this.ticketReplyService.deleteTicket(ticketReply.id).subscribe(() => this.afterDeletingReply())
    }

}