import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { FormGroup,FormBuilder } from '@angular/forms';

@Component({
    selector: 'reOpenTicket',
    templateUrl: './reOpenTicket.component.html'
})

export class ReopenTicketComponent implements OnInit{
    reopenTicketForm:FormGroup
    @Input() selectedTicket:Ticket
    @Output() ticketReopened: EventEmitter<string> =
    new EventEmitter<string>();
    private initialized:boolean = false
    errorMessage:any;
   
    constructor(private fb:FormBuilder, private ticketService:TicketService){

    }
    ngOnInit(){
       console.log( this.selectedTicket)
        this.reopenTicketForm = this.fb.group({
            ticketId:[''],
            reasonForReopening:['']
        });
        this.initialized = true       
    }

    ngOnChanges(){
        if(this.initialized){
            this.fillFormData()
        }
    }

    fillFormData(){
        this.reopenTicketForm = this.fb.group({
            ticketId:[this.selectedTicket.id],
            reasonForReopening:['']
        });
    }

    reopenTicket(event: any){
        
        let id:number = this.selectedTicket.id
        this.selectedTicket.status = "Reopened"
        this.selectedTicket.reasonForReopening = this.reopenTicketForm.get('reasonForReopening').value
        this.ticketService.updatingTicket(this.selectedTicket).subscribe(
            () => this.afterReopeningTicket(),
            (error:any) => this.errorMessage = error)
    }

    afterReopeningTicket() {
        this.ticketReopened.emit("Ticket Successfully Re-Opened!")
        alert('Ticket Successfully Re-Opened!')
        this.reopenTicketForm.setValue({
            ticketId:[''],
            reasonForReopening:['']
        });
        
    }

   

}