import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { TicketListComponent } from './ticket-list.component';
import { TicketService } from 'app/core/data-services/ticket.service';
import { CreateTicketComponent } from 'app/routes/tickets/createTicket.component';
import { TicketEditComponent } from 'app/routes/tickets/ticket-edit.component';
import { TicketDetailComponent } from 'app/routes/tickets/ticket-detail.component';
import { EmployeeService } from 'app/core/data-services/employee.service';
import { ClosedTicketListComponent } from 'app/routes/tickets/closedTicket-list.component';
import { ReopenTicketComponent } from 'app/routes/tickets/reOpenTicket.component';
import { TicketReplyBoxComponent } from 'app/routes/tickets/ticketReply-Box.component';
import { TicketReplyService } from 'app/core/data-services/ticket-reply.service';
import { MyTicketsComponent } from 'app/routes/tickets/myTickets.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs/tabs.module';
import { PagerService } from 'app/core/data-services/pager.service';
import { CloseTicketComponent } from 'app/routes/tickets/closeTicket.component';
import { ReviewInternalComponent } from 'app/routes/tickets/reviewInternal.component';
import { ReviewService } from 'app/core/data-services/review.service';

const routes: Routes = [
    //{ path: '', redirectTo: 'dashboard' },
    { path: '', redirectTo: 'activeTickets' },
    { path: 'activeTickets', component: TicketListComponent },
    { path: 'closedTickets', component: ClosedTicketListComponent },
    { path: 'myTickets', component: MyTicketsComponent }
    
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        TicketListComponent,
        CreateTicketComponent,
        TicketEditComponent,
        TicketDetailComponent,
        ClosedTicketListComponent,
        ReopenTicketComponent,
        TicketReplyBoxComponent,
        MyTicketsComponent,
        CloseTicketComponent,
        ReviewInternalComponent
    ],
    exports: [
        RouterModule
    ],
    providers:[
        TicketService, EmployeeService, TicketReplyService, PagerService, ReviewService
    ]
})
export class TicketModule { }