import { Component, OnInit, Input } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { DatePipe } from '@angular/common';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import { TableExport } from 'tableexport'
import { PagerService } from 'app/core/data-services/pager.service';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder } from '@angular/forms';

declare var $: any;

@Component({
    selector: 'ticket-list',
    templateUrl: './ticket-list.component.html',
    styles: [`
   `]
})

export class TicketListComponent implements OnInit {

    pageTitle: String = 'Active Tickets'
    isUserLoggedIn: boolean = false
    isUserAdmin: boolean = false
    tickets: Ticket[]
    selectedTicket: Ticket
    ticketDeleted: boolean = true
    errorMessage: any
    public tableExport: TableExport;
    private paginatedDisplay:boolean = true
    private filterForm:FormGroup

     pager: any = {};
    pagedItems: Ticket[];

    constructor(public userBlockService: UserblockService, 
                private pagerService:PagerService, 
                public authService: AuthService, 
                private ticketsService: TicketService,
                private fb:FormBuilder) {
    }
    ngOnInit(): void {
     
        if (this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined) {
            this.isUserLoggedIn = true
            if (this.userBlockService.getUserData().role == "Admin" ||
                this.userBlockService.getUserData().role == "Supervisor" ||
                this.userBlockService.getUserData().role == "Manager") {
                this.isUserAdmin = true
            }
        }
        this.filterForm = this.fb.group({
            filterOption:['Select Option'],
        });
        this.refreshList()   
    }

    export() {
        var instance = new TableExport(document.getElementById('allTicketList'),
            {
                headers: true,
                footers: true,
                formats: ['csv'],
                filename: 'TicketList',
                bootstrap: false,
                exportButtons: false,
                position: 'bottom',
                ignoreRows: null,
                ignoreCols: null,
                trimWhitespace: true
            });
        var exportDataCSV = instance.getExportData()['allTicketList']['csv'];
        instance.export2file(exportDataCSV.data, exportDataCSV.mimeType, exportDataCSV.filename, exportDataCSV.fileExtension);
        alert('Ticket list successfully exported!')
    }

    refreshList() {
        this.ticketsService.getTickets().map(tickets => tickets.filter(ticket => ticket.status !== "Closed"))
            .subscribe(tickets => {
                this.tickets = tickets
                this.setPage(1);
            },
            error => this.errorMessage = <any>error);
    }

    isTicketOverdue(ticket:Ticket){
        let today = new Date()
        let ticketDueDate = new Date(ticket.dueDate)
        if(ticketDueDate >= today){
            return false
        }else{
            return true
        }
    }

    deleteTicket(id: number) {
        if (this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined) {
            this.ticketsService.deleteTicket(id).subscribe(
                () => this.afterDeletingTicket(id),
                (error: any) => this.errorMessage = <any>error
            );
        } else {
            alert('You can\'t delete a task as a guest. Please Log-In!')
        }

    }

    afterDeletingTicket(id: number) {
        this.refreshList()
        if (this.selectedTicket != undefined && (id == this.selectedTicket.id || this.selectedTicket.status == "Closed")) {
            this.ticketDeleted = true
            
        } else {
            this.ticketDeleted = false
        }
        
        alert('Successfully Deleted!')
       
    }

    onTicketEdited() {
        this.refreshList()
        $('#editModal').modal('hide');
    }

    onTicketCreated() {
        this.refreshList()
        if(this.filterForm.get('filterOption').value != "Select Option" && this.filterForm.get('filterOption').value != ""){
            this.filterTicket()
        }
        $('#createModal').modal('hide');
    }

    onTicketClosed() {
        this.refreshList()
        this.ticketDeleted = true
    }

    setSelectedTicket(ticket: Ticket) {
        this.selectedTicket = ticket
        this.ticketDeleted = false
    }

    selectTicketToEdit(ticket: Ticket) {
        this.selectedTicket = ticket
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
    
        this.pager = this.pagerService.getPager(this.tickets.length, page);
        
        this.pagedItems = this.tickets.slice(this.pager.startIndex, this.pager.endIndex + 1);
       
    }

    filterTicket(){
       
        let filterOption:string = this.filterForm.get('filterOption').value
        if(filterOption == 'Today'){
        this.tickets = []
        this.pager.totalPages = 1
        var today = new Date()
        this.ticketsService.getTickets().map(tickets => tickets.filter(ticket => (ticket.status !== "Closed" && 
                                                                        new Date(ticket.created).getDate() == today.getDate() &&
                                                                        new Date(ticket.created).getMonth() == today.getMonth() &&
                                                                        new Date(ticket.created).getFullYear() == today.getFullYear()) 
                                                                    //    || (ticket.status == "Open")
                                                                    ))
        .subscribe(tickets => {
            this.tickets = tickets
            this.setPage(1);
        },
        error => this.errorMessage = <any>error);
        
        } 
        else if(filterOption == 'All'){
            this.tickets = []
           this.pager.totalPages = 1
           this.refreshList()
            
        }else if(filterOption == 'Yesterday'){
            this.tickets = []
            this.pager.totalPages = 1
            var today = new Date()
           
            this.ticketsService.getTickets().map(tickets => tickets.filter(ticket => ticket.status !== "Closed" && 
                                                                            new Date(ticket.created).getDate() == today.getDate()-1 &&
                                                                            new Date(ticket.created).getMonth() == today.getMonth() &&
                                                                            new Date(ticket.created).getFullYear() == today.getFullYear()))
            .subscribe(tickets => {
                this.tickets = tickets
                this.setPage(1);
            },
            error => this.errorMessage = <any>error);
        }else if(filterOption == 'This Month'){
            this.tickets = []
            this.pager.totalPages = 1
            var today = new Date()
            this.ticketsService.getTickets().map(tickets => tickets.filter(ticket => ticket.status !== "Closed" &&                                                                    
                                                                            new Date(ticket.created).getMonth() == today.getMonth() 
                                                                            ))
            .subscribe(tickets => {
                this.tickets = tickets
                this.setPage(1);
            },
            error => this.errorMessage = <any>error);
        }
        else{
            alert('Please select an filter option in order to carry out the ticket filtering process!')
        }
        
    }


}