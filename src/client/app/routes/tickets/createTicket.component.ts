import { Component, OnInit, EventEmitter, Output, OnChanges, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms'
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { EmployeeService } from 'app/core/data-services/employee.service';
import { Employee } from 'app/core/data-services/employee';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';


function priorityValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select a priority') {
            return { 'notSelected': true };
        };
        return null;
    };
}


function categoryValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select a category') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function dayValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'DD') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function monthValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'MM') {
            return { 'notSelected': true };
        };
        return null;
    };
}


function yearValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'YYYY') {
            return { 'notSelected': true };
        };
        return null;
    };
}

@Component({
    selector: 'create-ticket',
    templateUrl: './createTicket.component.html',
})

export class CreateTicketComponent implements OnInit {
    @Output() ticketCreated: EventEmitter<string> =
        new EventEmitter<string>();

    createTicketForm: FormGroup;
    employees: Employee[]
    private dayOptions: string[] = ["DD", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    private dueDayOptions: string[] = this.dayOptions
    private monthOptions: string[] = ["MM", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    private yearOptions: string[] = ["YYYY", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029"]
    private dueDate: Date
    private categoryList: string[] = ["Select a category", "Email", "Hardware", "Network", "Maintenance", "Printer", "Software", "Other"]
    private priorityList: string[] = ["Select a priority", "High", "Medium", "Low"]
    private ticket: Ticket = new Ticket();
    private employeeMap = new Map<number, string>();
    errorMessage: any;
    private dueDateInvalid:boolean = false

    constructor(private fb: FormBuilder, private ticketService: TicketService, private employeeService: EmployeeService, private u: UserblockService) {

    }
    ngOnInit() {
        this.employeeMap.set(0, 'Choose Employee/Assignee')
        this.employeeService.getEmployeeData()
            .subscribe(emp => this.employees = emp,
            (err) => console.error(err),

            () => {
                for (let i = 0; i < this.employees.length; i++) {
                    this.employeeMap.set(this.employees[i].id, this.employees[i].firstName)
                }
            })
        this.createTicketForm = this.fb.group({
            creator: [this.u.getUserData().name],
            summary: ['', [Validators.required, Validators.maxLength(50), Validators.minLength(3)]],
            description: ['', [Validators.required, Validators.maxLength(200), Validators.minLength(50)]],
            workSheetBody: ['', [Validators.required, Validators.minLength(50)]],
            assignee: [''],
            dueYear: ['YYYY', [yearValidator()]],
            dueMonth: ['MM', [monthValidator()]],
            dueDay: ['DD', [dayValidator()]],
            priority: ['Select a priority', [priorityValidator()]],
            category: ['Select a category', [categoryValidator()]],
        });

    }



    getEmployees() {
        return Array.from(this.employeeMap.values())
    }

    setDates() {
        this.dueDate = new Date(this.createTicketForm.get('dueYear').value,
            this.createTicketForm.get('dueMonth').value-1,
            this.createTicketForm.get('dueDay').value)
    }

    createTicket($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.createTicketForm.controls) {
            this.createTicketForm.controls[c].markAsTouched();
        }

        if (this.createTicketForm.valid) {
            this.setDates();
            let empId: number = 0
            let today: Date = new Date()

            if (this.dueDate >= today) {
                this.dueDateInvalid = false
                this.ticket.creatorId = this.u.getUserData().id
                this.ticket.creatorRole = this.u.getUserData().role
                this.ticket.creator = this.createTicketForm.get('creator').value
                this.ticket.summary = this.createTicketForm.get('summary').value
                this.ticket.workSheetBody = this.createTicketForm.get('workSheetBody').value
                this.ticket.description = this.createTicketForm.get('description').value

                if (this.createTicketForm.get('assignee').value == "Choose Employee/Assignee" ||
                    this.createTicketForm.get('assignee').touched == false ||
                    this.createTicketForm.get('assignee').value == "") {
                    this.ticket.assignee = null
                    this.ticket.assigneeEID = null
                } else {
                    this.ticket.assignee = this.createTicketForm.get('assignee').value
                    for (let i = 0; i < this.employeeMap.size; i++) {
                        if (this.employeeMap.get(i) == this.createTicketForm.get('assignee').value) {
                            empId = i
                        }
                    }
                    this.ticket.assigneeEID = empId
                }

                this.ticket.priority = this.createTicketForm.get('priority').value
                this.ticket.category = this.createTicketForm.get('category').value
                this.ticket.acceptStatus = false
                this.ticket.dueDate = this.dueDate.toJSON()
                this.ticket.progress = 0
                this.ticket.created = today.toJSON();

                this.ticket.lastUpdated = today.toJSON();

                this.ticket.status = 'Open'
                this.ticketService.saveTicket(this.ticket).subscribe(
                    () => this.afterCreatingNewTicket(),
                    (error: any) => this.errorMessage = <any>error
                );
            }else{
                this.dueDateInvalid = true
            }
        }
    }

    afterCreatingNewTicket() {
        for (let c in this.createTicketForm.controls) {
            this.createTicketForm.controls[c].markAsPristine();
        }
        for (let c in this.createTicketForm.controls) {
            this.createTicketForm.controls[c].markAsUntouched();
        }
        this.ticketCreated.emit("Ticket Successfully Created!")
        alert('Ticket Successfully Created!')
        this.dueDateInvalid = true
        this.createTicketForm.patchValue({
            creator: [this.u.getUserData().name],
            summary: [''],
            description: [''],
            workSheetBody: [''],
            dueYear: ['YYYY'],
            dueMonth: ['MM'],
            dueDay: ['DD'],
            priority: ['Select a priority'],
            category: ['Select a category'],
        });

    }

    modalClosed() {
        for (let c in this.createTicketForm.controls) {
            this.createTicketForm.controls[c].markAsPristine();
        }
        for (let c in this.createTicketForm.controls) {
            this.createTicketForm.controls[c].markAsUntouched();
        }
        this.dueDateInvalid = true

        this.createTicketForm.patchValue({
            creator: [this.u.getUserData().name],
            summary: [''],
            description: [''],
            workSheetBody: [''],
            dueYear: ['YYYY'],
            dueMonth: ['MM'],
            dueDay: ['DD'],
            priority: ['Select a priority'],
            category: ['Select a category'],
        });
    }

    dueDayChange(input: number, event: any) {
        var previousData = this.createTicketForm.get('dueDay').value
        if (input == 2) {
            this.dueDayOptions = this.dayOptions.slice(0, 29)
            this.createTicketForm.get('dueDay').updateValueAndValidity();

            if (previousData == 30 || previousData == 31 || previousData == "") {

                this.createTicketForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.createTicketForm.get('dueDay').setValue(previousData);
            }
        } else if (input == 9 || input == 4 || input == 6 || input == 11) {
            this.dueDayOptions = this.dayOptions.slice(0, 31)
            this.createTicketForm.get('dueDay').updateValueAndValidity();

            if (previousData == 31 || previousData == "") {
                this.createTicketForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.createTicketForm.get('dueDay').setValue(previousData);
            }
        } else {
            this.dueDayOptions = this.dayOptions.slice(0, 32)
            this.createTicketForm.get('dueDay').updateValueAndValidity();
            if (previousData == "") {
                this.createTicketForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.createTicketForm.get('dueDay').setValue(previousData);
            }
        }
    }

    fileChange(event) {

        let fileList: FileList = event.target.files;
        console.log(fileList)
        if (fileList.length > 0) {
            this.ticketService.uploadFile(fileList)
        }
    }

}