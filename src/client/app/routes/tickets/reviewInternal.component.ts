import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Ticket } from 'app/core/data-services/ticket';
import { FormGroup,FormBuilder } from '@angular/forms/';
import { Review } from 'app/core/data-services/review';

import { ReviewService } from 'app/core/data-services/review.service';
import { TicketService } from 'app/core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';

@Component({
    selector: 'reviewInternal-component',
    templateUrl: './reviewInternal.component.html'
})

export class ReviewInternalComponent implements OnInit{
    @Input() ticket:Ticket 

    isUserLoggedIn:boolean = false
    errorMessage:any

    reviewList:Review[]

    constructor(private userBlockService:UserblockService, private ticketService:TicketService, private fb:FormBuilder, private reviewService:ReviewService){
    }

    ngOnInit(){
        if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
            this.isUserLoggedIn = true
        } 
        
        
    }

    ngOnChanges(){
        this.reviewService.getReviews().map(reviews => reviews.filter(review => review.ticketId == this.ticket.id))
        .subscribe(reviews => this.reviewList = reviews,
                    error => this.errorMessage = <any>error,
                  () => console.log(this.reviewList));
    }

}