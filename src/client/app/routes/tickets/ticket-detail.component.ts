import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Ticket } from '../../core/data-services/ticket';
import { TicketService } from '../../core/data-services/ticket.service';
import { DatePipe } from '@angular/common';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';

import { FormGroup, FormBuilder } from '@angular/forms/';
declare var $:any

@Component({
    selector: 'tickets-detail',
    templateUrl: './ticket-detail.component.html'
})

export class TicketDetailComponent implements OnInit {
    imageUrl: string
    @Input() ticket: Ticket
    @Input() ticketDeleted: boolean
    @Output() ticketClosed: EventEmitter<string> = new EventEmitter<string>();
    private isUserLoggedIn: boolean = false
    private isUserPrivileged: boolean = false
    private inputTicket: Ticket;
    private initialized: boolean = false
    errorMessage: any

    constructor(private userBlockService: UserblockService, private ticketService: TicketService, private fb: FormBuilder) {

    }
    ngOnInit() {
        if (this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined) {
            this.isUserLoggedIn = true
            if(this.userBlockService.getUserData().role == "Admin" ||
            this.userBlockService.getUserData().role == "Supervisor" ||
            this.userBlockService.getUserData().role == "Manager"){
             this.isUserPrivileged = true
         }
        }
        this.imageUrl = "/assets/img/angular.svg";
        this.initialized = true
    }
    ngOnChanges() {
        if (this.initialized) {
            
            this.inputTicket = this.ticket
        }
    }


    afterClosingTicket() {
        this.ticketClosed.emit("Ticket Successfully Closed!")
        alert("Ticket Successfully Closed!")
    }

    onTicketClosed(){
        $('#closeTicketModal').modal('hide');
        this.afterClosingTicket()
        this.ticketDeleted = true
    }

}