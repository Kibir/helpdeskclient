import { Component, OnInit, OnChanges } from '@angular/core';
import { Http } from '@angular/http';

import { ColorsService } from '../../../shared/colors/colors.service';
import { ChartService } from 'app/core/data-services/chart.service';
import { TicketService } from 'app/core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';

import { Ticket } from 'app/core/data-services/ticket';

import { ActivatedRoute } from '@angular/router';
import { Employee } from 'app/core/data-services/employee';
import { Customer } from 'app/core/data-services/customer';
import { Article } from 'app/core/data-services/article';

@Component({
    selector: 'app-dashboardv1',
    templateUrl: './dashboardv1.component.html',
    styleUrls: ['./dashboardv1.component.scss']
})
export class Dashboardv1Component implements OnInit {
    private ticketList:Ticket[]
    private userList:Employee[]
    private customerList:Customer[]
    private articleList:Article[]

    private ticketsRetrieved:boolean = false
    private usersRetrieved:boolean = false
    private customerRetrieved:boolean = false
    private articleRetrieved:boolean = false

    private yourTickets:number = 0
    private openedTickets:number = 0
    private unassignedTickets:number = 0
    private numOfCustomers:number = 0
    private numOfArticles:number = 0
    private numOfEmployees:number = 0
    easyPiePercent: number = 70;
    pieOptions = {
        animate: {
            duration: 800,
            enabled: true
        },
        barColor: this.colors.byName('info'),
        trackColor: 'rgba(200,200,200,0.4)',
        scaleColor: false,
        lineWidth: 10,
        lineCap: 'round',
        size: 145
    };

    sparkOptions1 = {
        barColor: this.colors.byName('info'),
        height: 30,
        barWidth: '5',
        barSpacing: '2'
    };

    sparkOptions2 = {
        type: 'line',
        height: 80,
        width: '100%',
        lineWidth: 2,
        lineColor: this.colors.byName('purple'),
        spotColor: '#888',
        minSpotColor: this.colors.byName('purple'),
        maxSpotColor: this.colors.byName('purple'),
        fillColor: '',
        highlightLineColor: '#fff',
        spotRadius: 3,
        resize: true
    };

    splineHeight = 280;
    splineData: any;
    splineOptions = {
        series: {
            lines: {
                show: false
            },
            points: {
                show: true,
                radius: 4
            },
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.5
            }
        },
        grid: {
            borderColor: '#eee',
            borderWidth: 1,
            hoverable: true,
            backgroundColor: '#fcfcfc'
        },
        tooltip: true,
        tooltipOpts: {
            content: (label, x, y) => { return x + ' : ' + y; }
        },
        xaxis: {
            tickColor: '#fcfcfc',
            mode: 'categories'
        },
        yaxis: {
            min: 0,
            max: 150, // optional: use it for a clear represetation
            tickColor: '#eee',
            // position: ($scope.app.layout.isRTL ? 'right' : 'left'),
            tickFormatter: (v) => {
                return v/* + ' visitors'*/;
            }
        },
        shadowSize: 0
    };

    constructor(public colors: ColorsService, 
                public chartService:ChartService, 
                private ticketService: TicketService, 
                private userBlockService:UserblockService,
                private route: ActivatedRoute

    ) {
        //http.get('api/spline').map(data => data.json()).subscribe(data => this.splineData = data);
        chartService.getChartData().subscribe(data => this.splineData = data);
    }

    ngOnInit() {
        this.route.data
        .subscribe(params => {
            let list: Ticket[] = params['tickets']
            this.ticketList = list
            this.ticketsRetrieved = true
        });
        this.route.data
        .subscribe(params => {
            let list: Employee[] = params['user']
            this.userList = list
            this.usersRetrieved = true
        });
        this.route.data
        .subscribe(params => {
            let list: Customer[] = params['customer']
            this.customerList = list
            this.customerRetrieved = true
        });
        this.route.data
        .subscribe(params => {
            let list: Article[] = params['article']
            this.articleList = list
            this.articleRetrieved = true
        });
        this.ngOnChanges()
      
     }

     ngOnChanges(){
        
         if(this.ticketsRetrieved)
         {
             this.refreshTicketCount()
         }

         if(this.usersRetrieved){
            this.refreshUserCount()
         }

         if(this.articleRetrieved){
            this.refreshArticleCount()
         }
     }

    colorByName(name) {
        return this.colors.byName(name);
    }

    refreshTicketCount(){
       
            this.ticketList.forEach(ticket => {
               
                if(ticket.assignee == this.userBlockService.getUserData().name){
                    this.yourTickets += 1
                }
                if(ticket.assignee == "Unassigned"){
                    this.unassignedTickets += 1
                }
                if(ticket.status == "Open"){
                    this.openedTickets += 1
                }
            });
        
    }

    refreshUserCount(){
           this.numOfEmployees =this.userList.length
    }

    refreshArticleCount(){
        this.numOfArticles =this.articleList.length
 }

}
