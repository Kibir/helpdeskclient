import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import {ContactListComponent} from '../../routes/contacts/contact-list.component'
import { EmployeeService } from 'app/core/data-services/employee.service';


const routes: Routes = [
    { path: '', component: ContactListComponent },
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
      ContactListComponent
    ],
    exports: [
        RouterModule
    ],
    providers:[
        EmployeeService
    ]
})
export class ContactModule { }