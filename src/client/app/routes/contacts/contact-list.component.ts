import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'app/core/data-services/employee.service';

@Component({
    selector: 'contact-list',
    templateUrl: './contact-list.component.html',
    styles: [`
   `]
})

export class ContactListComponent implements OnInit{
    pageTitle:String = 'Contacts'
    employeeUser:Employee
    employees:Employee[]
    isUserLoggedIn:boolean = false
    errorMessage:any

    constructor(public userBlockService:UserblockService,private route:ActivatedRoute, private employeeService:EmployeeService, private authService:AuthService){
        
    }
    ngOnInit(): void {
        
       if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
           this.isUserLoggedIn = true
       }
       
        this.authService.getUser(this.userBlockService.getUserData().id)
        .subscribe(user => this.employeeUser = user)     
        this.employeeService.getEmployeeData().map(employees => employees.filter(e => e.id != 0)).subscribe(employee => this.employees = employee,
            error => this.errorMessage = <any>error)
    }
}