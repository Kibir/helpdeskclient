import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { SubmittedTicketsComponent } from 'app/routes/home/home/submitted-tickets.component';
import { TicketService } from 'app/core/data-services/ticket.service';
import { SubmitNewCaseComponent } from 'app/routes/home/home/submitNewCase.component';
import { DisplaySearchDataComponent } from 'app/routes/home/home/displaySearchData.component';
import { CustomerCaseDetailComponent } from 'app/routes/home/home/customerCaseDetail.component';
import { ReviewComponent } from 'app/routes/home/home/review.component';
import { ReviewService } from 'app/core/data-services/review.service';
import { PagerService } from 'app/core/data-services/pager.service';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'search', component: DisplaySearchDataComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule
    ],
    declarations: [HomeComponent, 
                   SubmittedTicketsComponent, 
                   SubmitNewCaseComponent,
                   DisplaySearchDataComponent,
                   CustomerCaseDetailComponent,
                   ReviewComponent],
    exports: [
        RouterModule
    ],
    providers:[
        TicketService, ReviewService, PagerService
    ]
})
export class HomeModule { }