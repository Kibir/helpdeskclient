import { Component, OnInit } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TicketListComponent } from 'app/routes/tickets/ticket-list.component';
import { Router } from '@angular/router';

declare var $:any

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    //styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    user:any;
    eligibility:boolean = true
    searchCaseForm:FormGroup
    selectedTicketId:number = 0
    private isUserLoggedIn:boolean = false
    private contentUpdated:boolean = false
    constructor(private userBlockService:UserblockService,private fb:FormBuilder, private _router:Router) {
     }

    ngOnInit() {
        this.searchCaseForm = this.fb.group({
            searchData:['']
        })
        if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
            this.isUserLoggedIn = true
        }
    }

    checkIfUserIsGuest():boolean{
        if(this.userBlockService.getUserData().id == 0 || this.userBlockService.getUserData().id == undefined){
            return true
        }else{
            return false
        }
    }

    afterCaseCreated(){
        alert('A new case was created!')
        $('#createNewCaseModal').modal('hide');
        this.contentUpdated = true
    }

    evokeSearch(){
        let searchData:string = this.searchCaseForm.get('searchData').value
        this._router.navigate(['/home/search',{sd:searchData}])
    }

    showDetail(event){
        this.selectedTicketId = event
    }


}
