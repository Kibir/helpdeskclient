import { Component, OnInit } from '@angular/core';
import { Ticket } from '../../../core/data-services/ticket';
import { TicketService } from '../../../core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import { TableExport } from 'tableexport'
import { PagerService } from 'app/core/data-services/pager.service';

declare var $:any;

@Component({
    selector: 'display-search',
    templateUrl: './displaySearchData.component.html',

})

export class DisplaySearchDataComponent implements OnInit{
   
    pageTitle:String = 'Search Results'
    isUserLoggedIn:boolean = false
    tickets:Ticket[]

    selectedTicket:Ticket
    errorMessage:any
    pager: any = {};
    pagedItems: Ticket[];

    public tableExport: TableExport;

    constructor(public userBlockService:UserblockService, 
                private ticketService:TicketService, 
                private route:ActivatedRoute,
                private pagerService:PagerService){ 
    }
    ngOnInit(): void {
      let searchData = this.route.snapshot.paramMap.get('sd')
      this.search(searchData)
    }

     search(sd:string){
         console.log(this.route.snapshot.paramMap.get('sd'))
        this.ticketService.getTickets().map(tickets => tickets.filter(ticket => ticket.creatorCustomerName == this.userBlockService.getUserData().name))
        .map(tickets => tickets.filter(ticket => ticket.summary.toLocaleLowerCase().indexOf(sd.toLocaleLowerCase()) >= 0))
        .subscribe(tickets => this.tickets = tickets,
                   error => this.errorMessage = <any>error,
                   () => {
                      if(this.tickets.length > 0){ 
                          this.setPage(1)
                        }
                    });
    } 

   
    setSelectedTicket(ticket:Ticket)
    {
        this.selectedTicket = ticket
    }    

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
    
        this.pager = this.pagerService.getPager(this.tickets.length, page);
        
        this.pagedItems = this.tickets.slice(this.pager.startIndex, this.pager.endIndex + 1);
       
    }

   
}