import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Ticket } from '../../../core/data-services/ticket';
import { TicketService } from '../../../core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { AuthService } from 'app/core/data-services/auth.service';
import { Employee } from 'app/core/data-services/employee';
import { ActivatedRoute } from '@angular/router';
import { TableExport } from 'tableexport'
import { PagerService } from 'app/core/data-services/pager.service';

declare var $:any;

@Component({
    selector: 'submitted-tickets',
    templateUrl: './submitted-tickets.component.html',
})

export class SubmittedTicketsComponent implements OnInit{
    @Input() contentUpdated:boolean
    @Output() selectedId: EventEmitter<number> = new EventEmitter<number>();
  
    pageTitle:String = 'Submitted Tickets'
    employeeUser:Employee
    isUserLoggedIn:boolean = false
    listFilter:String = ''
    tickets:Ticket[]
    selectedTicket:Ticket
    ticketDeleted:boolean = true
    errorMessage:any

    public tableExport: TableExport;

    pager: any = {};
    pagedItems: Ticket[];


    constructor(public userBlockService:UserblockService, 
                public authService:AuthService, 
                private ticketsService:TicketService,
                private pagerService:PagerService){ 
    }
    ngOnInit(): void {
     
        this.generateCreatorSpecificList(this.userBlockService.getUserData().name)
        
        this.authService.getUser(this.userBlockService.getUserData().id)
        .subscribe(user => this.employeeUser = user) 
        this.validateUser()
    }

    ngOnChanges(){
        if(this.contentUpdated){
           
            this.generateCreatorSpecificList(this.userBlockService.getUserData().name)

        }
    }

    validateUser(){
        if(this.userBlockService.getUserData().role == 'Customer'){
            this.isUserLoggedIn = true
        }
    }

    export(){
       var instance =  new TableExport(document.getElementById('allTicketList'),       
       {
        headers: true,                              
        footers: true,                              
        formats: ['csv'],            
        filename: 'TicketList',                            
        bootstrap: false,                         
        exportButtons: false,                       
        position: 'bottom',                       
        ignoreRows: null,                
        ignoreCols: [9,10],                           
        trimWhitespace: true                       
    });
       var exportDataCSV = instance.getExportData()['allTicketList']['csv'];
       
       instance.export2file(exportDataCSV.data, exportDataCSV.mimeType, exportDataCSV.filename, exportDataCSV.fileExtension);
        alert('Ticket list successfully exported!')
    }

   
     generateCreatorSpecificList(creator:string){
        this.ticketsService.getTickets().map(tickets => tickets.filter(ticket => ticket.creatorCustomerName == creator))
        .subscribe(tickets => 
        this.tickets = tickets,
        error => this.errorMessage = <any>error,
        () =>
        { 
            if(this.tickets.length > 0){
                this.setPage(1)
            }
        
        }
    );
    } 

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
    
        this.pager = this.pagerService.getPager(this.tickets.length, page);
        
        this.pagedItems = this.tickets.slice(this.pager.startIndex, this.pager.endIndex + 1);
       
    }

    setSelectedTicket(ticket:Ticket)
    {
        this.selectedTicket = ticket
        this.selectedId.emit(this.selectedTicket.id)
    }    

   
}