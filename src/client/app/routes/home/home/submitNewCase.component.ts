import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms'
import { Ticket } from '../../../core/data-services/ticket';
import { TicketService } from '../../../core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';

declare var $: any

function priorityValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select a priority') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function categoryValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'Select a category') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function dayValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'DD') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function monthValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'MM') {
            return { 'notSelected': true };
        };
        return null;
    };
}

function yearValidator(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
        if (c.value == '' || c.value == 'YYYY') {
            return { 'notSelected': true };
        };
        return null;
    };
}

@Component({
    selector: 'new-case',
    templateUrl: './submitNewCase.component.html',
})

export class SubmitNewCaseComponent implements OnInit {

    @Output() caseCreated: EventEmitter<string> =
        new EventEmitter<string>();

    createNewCaseForm: FormGroup;
    private dayOptions: string[] = ["DD", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    private dueDayOptions: string[] = this.dayOptions
    private monthOptions: string[] = ["MM", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    private yearOptions: string[] = ["YYYY", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029"]
    private dueDate: Date
    private categoryList: string[] = ["Select a category", "Email", "Hardware", "Network", "Maintenance", "Printer", "Software", "Other"]
    private priorityList: string[] = ["Select a priority", "High", "Medium", "Low"]
    private ticket: Ticket = new Ticket()
    errorMessage: any
    private dueDateInvalid:boolean = false

    constructor(private fb: FormBuilder, private ticketService: TicketService, private u: UserblockService) {

    }
    ngOnInit() {

        this.createNewCaseForm = this.fb.group({
            creator: [this.u.getUserData().name],
            summary: ['', [Validators.required, Validators.maxLength(50), Validators.minLength(3)]],
            description: ['', [Validators.required, Validators.maxLength(200), Validators.minLength(50)]],
            workSheetBody: ['', [Validators.required, Validators.minLength(50)]],
            dueYear: ['', [yearValidator()]],
            dueMonth: ['', [monthValidator()]],
            dueDay: ['', [dayValidator()]],
            priority: ['', [priorityValidator()]],
            category: ['', [categoryValidator()]],
        });
    }

    setDates() {
        this.dueDate = new Date(this.createNewCaseForm.get('dueYear').value,
            this.createNewCaseForm.get('dueMonth').value-1,
            this.createNewCaseForm.get('dueDay').value)
    }


    createTicket($ev, value: any) {
        $ev.preventDefault();
        this.setDates();
        for (let c in this.createNewCaseForm.controls) {
            this.createNewCaseForm.controls[c].markAsTouched();
        }

       
        if (this.u.getUserData().id != 0 && this.u.getUserData().role != 'Guest') {
            if (this.createNewCaseForm.valid) {  
                
                this.ticket.creatorCustomerId = this.u.getUserData().id
                this.ticket.creatorCustomerName = this.createNewCaseForm.get('creator').value
                this.ticket.summary = this.createNewCaseForm.get('summary').value
                this.ticket.description = this.createNewCaseForm.get('description').value
                this.ticket.assignee = null
                this.ticket.assigneeEID = null
                this.ticket.creator = null
                this.ticket.creatorId = null
                this.ticket.creatorRole = null
                this.ticket.priority = this.createNewCaseForm.get('priority').value
                this.ticket.category = this.createNewCaseForm.get('category').value
                this.ticket.workSheetBody = this.createNewCaseForm.get('workSheetBody').value
                this.ticket.acceptStatus = false
                this.ticket.dueDate = this.dueDate.toDateString()
                this.ticket.progress = 0
                this.ticket.created = new Date().toDateString();
                this.ticket.lastUpdated = new Date().toDateString();
                this.ticket.status = 'Open'
        
                this.ticketService.saveTicket(this.ticket).subscribe(
                    () => this.afterCreatingNewTicket(),
                    (error: any) => this.errorMessage = <any>error
                );
            }
        
        } else {
            this.createNewCaseForm.setValue({
                creator: [this.u.getUserData().name],
                summary: [''],
                description: [''],
                workSheetBody: [''],
                dueYear: ['YYYY'],
                dueMonth: ['MM'],
                dueDay: ['DD'],
                priority: ['Select a priority'],
                category: ['Select a category'],
            });
            for (let c in this.createNewCaseForm.controls) {
                this.createNewCaseForm.controls[c].markAsPristine();
            }
            for (let c in this.createNewCaseForm.controls) {
                this.createNewCaseForm.controls[c].markAsUntouched();
            }
            $('#createNewCaseModal').modal('hide');
            alert('You can\'t create or submit a case as a guest! Please Log-in!')
        }
    }

    afterCreatingNewTicket() {
        this.caseCreated.emit("New Case Successfully Created!")
        this.createNewCaseForm.setValue({
            creator: [this.u.getUserData().name],
            summary: [''],
            description: [''],
            workSheetBody: [''],
            dueYear: ['YYYY'],
            dueMonth: ['MM'],
            dueDay: ['DD'],
            priority: ['Select a priority'],
            category: ['Select a category'],
        });
        for (let c in this.createNewCaseForm.controls) {
            this.createNewCaseForm.controls[c].markAsPristine();
        }
        for (let c in this.createNewCaseForm.controls) {
            this.createNewCaseForm.controls[c].markAsUntouched();
        }
    }

    dueDayChange(input: number, event: any) {
        var previousData = this.createNewCaseForm.get('dueDay').value
        if (input == 2) {
            this.dueDayOptions = this.dayOptions.slice(0, 29)
            this.createNewCaseForm.get('dueDay').updateValueAndValidity();

            if (previousData == 30 || previousData == 31 || previousData == "") {

                this.createNewCaseForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.createNewCaseForm.get('dueDay').setValue(previousData);
            }
        } else if (input == 9 || input == 4 || input == 6 || input == 11) {
            this.dueDayOptions = this.dayOptions.slice(0, 31)
            this.createNewCaseForm.get('dueDay').updateValueAndValidity();

            if (previousData == 31 || previousData == "") {
                this.createNewCaseForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.createNewCaseForm.get('dueDay').setValue(previousData);
            }
        } else {
            this.dueDayOptions = this.dayOptions.slice(0, 32)
            this.createNewCaseForm.get('dueDay').updateValueAndValidity();
            if (previousData == "") {
                this.createNewCaseForm.get('dueDay').setValue(this.dueDayOptions[0]);
            } else {
                this.createNewCaseForm.get('dueDay').setValue(previousData);
            }
        }
    }

    modalClosed(){
        this.createNewCaseForm.setValue({
            creator: [this.u.getUserData().name],
            summary: [''],
            description: [''],
            workSheetBody: [''],
            dueYear: ['YYYY'],
            dueMonth: ['MM'],
            dueDay: ['DD'],
            priority: ['Select a priority'],
            category: ['Select a category'],
        });
        for (let c in this.createNewCaseForm.controls) {
            this.createNewCaseForm.controls[c].markAsPristine();
        }
        for (let c in this.createNewCaseForm.controls) {
            this.createNewCaseForm.controls[c].markAsUntouched();
        }
    }

}