import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Ticket } from '../../../core/data-services/ticket';
import { TicketService } from '../../../core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { FormGroup,FormBuilder } from '@angular/forms/';

@Component({
    selector: 'customerCase-detail',
    templateUrl: './customerCaseDetail.component.html'
})

export class CustomerCaseDetailComponent implements OnInit{
   imageUrl:string 
    @Input() ticketId:number
    @Output() reviewSubmitted:EventEmitter<any> = new EventEmitter<any>() 
    isUserLoggedIn:boolean = false
    private inputTicket:Ticket;
    errorMessage:any

    constructor(private userBlockService:UserblockService, private ticketService:TicketService, private fb:FormBuilder){

    }
    ngOnInit(){
        if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
            this.isUserLoggedIn = true
        } 
        this.imageUrl = "/assets/img/angular.svg";
        
    }
    ngOnChanges(){
        this.ticketService.getTicket(this.ticketId).subscribe(ticket => this.inputTicket = ticket)
        //this.inputTicket = this.ticket 
    }

}