import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Ticket } from '../../../core/data-services/ticket';
import { FormGroup,FormBuilder } from '@angular/forms/';
import { Review } from 'app/core/data-services/review';

import { ReviewService } from 'app/core/data-services/review.service';
import { TicketService } from '../../../core/data-services/ticket.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';

@Component({
    selector: 'review-component',
    templateUrl: './review.component.html'
})

export class ReviewComponent implements OnInit{
    @Input() id:number 
    @Output() newReviewSubmitted:EventEmitter<string> = new EventEmitter<string>();
    isUserLoggedIn:boolean = false
    private iTicket:Ticket;
    errorMessage:any
    reviewForm:FormGroup
    newReview:Review = new Review()
    reviewList:Review[]

    constructor(private userBlockService:UserblockService, private ticketService:TicketService, private fb:FormBuilder, private reviewService:ReviewService){
    }

    ngOnInit(){
        this.reviewForm = this.fb.group({
            satisfactionLvl:['Satisfaction Level'],
            description:['']
        })
        if(this.userBlockService.getUserData().id != 0 && this.userBlockService.getUserData().id != undefined){
            this.isUserLoggedIn = true
        } 
        
        
    }

    ngOnChanges(){
        this.ticketService.getTicket(this.id).subscribe(ticket => this.iTicket = ticket)
        this.reviewService.getReviews().map(reviews => reviews.filter(review => review.ticketId == this.iTicket.id))
        .subscribe(reviews => this.reviewList = reviews,
                    error => this.errorMessage = <any>error,
                  () => console.log(this.reviewList));
    }

    saveReview(){
        this.newReview.id = 5
        this.newReview.ticketId = this.iTicket.id
        this.newReview.description = this.reviewForm.get('description').value
        this.newReview.postedDateTime = new Date().toDateString()
        this.newReview.reviewerId = this.userBlockService.getUserData().id
        this.newReview.reviewerName = this.userBlockService.getUserData().name
        
        if(this.reviewForm.get('satisfactionLvl').value == 'Poor'){
            this.newReview.rating = 1
        }else if(this.reviewForm.get('satisfactionLvl').value == 'Ok'){
            this.newReview.rating = 2
        }else if(this.reviewForm.get('satisfactionLvl').value == 'Average'){
            this.newReview.rating = 3
        }else if(this.reviewForm.get('satisfactionLvl').value == 'Delightful'){
            this.newReview.rating = 4
        }else if(this.reviewForm.get('satisfactionLvl').value == 'Amazed'){
            this.newReview.rating = 5
        }else{
            this.newReview.rating = 5
        }

        this.reviewService.saveReview(this.newReview).subscribe(() => this.afterReviewSubmitted())
    }

    editReview(){

    }

    afterReviewSubmitted(){
        alert('A new review submitted!')
       this.ngOnChanges()
    }

}