import { LayoutComponent } from '../layout/layout.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';
import { WaitforUserLoad } from 'app/core/data-services/waitforUserLoad.resolve';
import { WaitForCustomerLoad } from 'app/core/data-services/waitforCustomerLoad.resolve';
import { CanActivate } from '@angular/router';
import { 
    AuthGuardService as AuthGuard 
  } from '../core/routeGuards/customerAuthGuard.service';
import { WaitForTicketLoad } from 'app/core/data-services/waitforTicketLoad.resolve';
import { WaitForArticleLoad } from 'app/core/data-services/waitForArticleLoad.resolve';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule'},
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate:[AuthGuard] , resolve: {
                tickets: WaitForTicketLoad, user: WaitforUserLoad, customer:WaitForCustomerLoad, article: WaitForArticleLoad}
              },
            { path: 'widgets', loadChildren: './widgets/widgets.module#WidgetsModule' },
            { path: 'elements', loadChildren: './elements/elements.module#ElementsModule' },
            { path: 'forms', loadChildren: './forms/forms.module#FormsModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'maps', loadChildren: './maps/maps.module#MapsModule' },
            { path: 'blog', loadChildren: './blog/blog.module#BlogModule' },
            { path: 'ecommerce', loadChildren: './ecommerce/ecommerce.module#EcommerceModule' },
            { path: 'extras', loadChildren: './extras/extras.module#ExtrasModule' },
            { path: 'tickets', loadChildren: './tickets/ticket.module#TicketModule',  canActivate:[AuthGuard] },
            { path: 'contacts', loadChildren: './contacts/contact.module#ContactModule', canActivate:[AuthGuard]  },
            { path: 'employees', loadChildren: './employee/employee.module#EmployeeModule', canActivate:[AuthGuard] },
            { path: 'knowledgeBase', loadChildren: './knowledgeBase/knowledge.module#KnowledgeModule', canActivate:[AuthGuard] }
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent, resolve: {
        user: WaitforUserLoad, customer:WaitForCustomerLoad
      } },
    { path: 'register', component: RegisterComponent },
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: 'home' }

];
