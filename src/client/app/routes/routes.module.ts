import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
//import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { SharedModule } from '../shared/shared.module';
import { PagesModule } from './pages/pages.module';

import { menu } from './menu';
import { routes } from './routes';
import { WaitforUserLoad } from 'app/core/data-services/waitforUserLoad.resolve';
import { WaitForCustomerLoad } from 'app/core/data-services/waitforCustomerLoad.resolve';
import { WaitForTicketLoad } from 'app/core/data-services/waitforTicketLoad.resolve';
import { WaitForArticleLoad } from 'app/core/data-services/waitForArticleLoad.resolve';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forRoot(routes),
        PagesModule
    ],
    declarations: [],
    exports: [
        RouterModule
    ],
    providers:[WaitforUserLoad, WaitForCustomerLoad, WaitForTicketLoad, WaitForArticleLoad]
})

export class RoutesModule {
    constructor(public menuService: MenuService ) {
        menuService.addMenu(menu);    
    }

 

   
}
