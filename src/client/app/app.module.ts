import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';

import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';
import { RoutesModule } from './routes/routes.module';

// import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { DataProvider } from './core/data-services/dataProvider';
import { AuthService } from 'app/core/data-services/auth.service';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';
import { CustomerService } from 'app/core/data-services/customer.service';
import { TicketListComponent } from 'app/routes/tickets/ticket-list.component';
import { TicketService } from 'app/core/data-services/ticket.service';
import { AuthGuardService } from 'app/core/routeGuards/customerAuthGuard.service';
import { ArticleService } from 'app/core/data-services/article.service';


//import { TranslateStore } from '@ngx-translate/core/src/translate.store';
// https://github.com/ocombe/ng2-translate/issues/218
/* export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
} */

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpModule,
        BrowserAnimationsModule,
        CoreModule,
        LayoutModule,
        SharedModule.forRoot(),
        RoutesModule,
        // InMemoryWebApiModule.forRoot(DataProvider)
    ],
    exports:[],
    providers: [AuthService,UserblockService, CustomerService, TicketService, AuthGuardService, ArticleService],
    bootstrap: [AppComponent]
})
export class AppModule { }
