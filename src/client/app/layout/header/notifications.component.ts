import { Component, OnInit, ViewChild } from '@angular/core';

import { UserblockService } from '../sidebar/userblock/userblock.service';
import { SettingsService } from '../../core/settings/settings.service';
import { MenuService } from '../../core/menu/menu.service';

@Component({
    selector: 'notifications',
    templateUrl: './notifications.component.html',
})
export class NotificationComponent implements OnInit {

    constructor(public menu: MenuService, public userblockService: UserblockService, public settings: SettingsService) {

      

    }

    ngOnInit() {
        
    }

}
