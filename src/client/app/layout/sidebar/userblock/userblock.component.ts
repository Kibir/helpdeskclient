import { Component, OnInit } from '@angular/core';

import { UserblockService } from './userblock.service';

import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: any;
    constructor(public userblockService: UserblockService) {
        
           /*  this.user = {
                name:'John',
                picture: 'assets/img/user/02.jpg'
            };
        */
        this.user = this.userblockService.getUserData()
      
}

    ngOnInit() {
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

}
