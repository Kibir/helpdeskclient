import { Injectable } from '@angular/core';

@Injectable()
export class UserblockService {
    public userBlockVisible: boolean;
    public user:any
    constructor() {
        // initially visible
        this.userBlockVisible  = true;
    }

    getVisibility() {
        return this.userBlockVisible;
    }

    setVisibility(stat = true) {
        this.userBlockVisible = stat;
    }

    toggleVisibility() {
        this.userBlockVisible = !this.userBlockVisible;
    }

    setUserData(user:any){
        this.user = user
    }

    getUserData(){
        if(this.user == null || this.user == undefined){
            this.user = {
                id:0,
                name:'Guest',
                role:'Guest',
                picture: 'assets/img/user/01.jpg'
            };
        }
        return this.user
    }

}
