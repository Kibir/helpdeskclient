import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    styles:[`.wrapper {
        position: relative;
        width: 100%;
        height: auto;
        min-height: 100%;
        overflow-x: hidden;
        z-index: 1;
    }`]
})
export class LayoutComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
