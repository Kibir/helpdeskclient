import { Certificate } from "app/core/data-services/certificate";
import { Degree } from "app/core/data-services/degree";

export class ProfessionByEmployee{
    id: number
    eId: number
    certificates:Certificate[]
    degrees:Degree[]

  constructor(){
      
  }
}