export class Ticket{
    id: number
    description: string
    summary:string
    workSheetBody:string
    assignee:string
    assigneeEID:number
    acceptStatus:boolean
    category:string
    creatorId:number
    creator:string
    creatorRole:string
    creatorCustomerId:number
    creatorCustomerName:string
    priority:string
    dueDate:string
    lastUpdated:string
    created:string
    lastActivity:string
    status:string
    progress:number
    reasonForClosing:string
    reasonForReopening:string
  constructor(){
      
  }
}