import { Reference } from "app/core/data-services/reference"

export class Article {
    id: number
    title:string
    body:string
    conclusionBody:string
    postedDate:string
    lastUpdatedDate:string
    postedUserId:number
    postedUserName:string
    references:Reference[]

  constructor(){
      
  }
}