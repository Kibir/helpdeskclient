import { Injectable, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';

import { Employee } from 'app/core/data-services/employee';

@Injectable()
export class EmployeeService {
    private baseUrl = 'http://qshelpdeskapi.azurewebsites.net/api/employee/';

    constructor(private http: Http) { }

    getEmployeeData(): Observable<Employee[]> {
        
        return this.http.get(`${this.baseUrl}list/`).debounceTime(400)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getSpecificEmployee(id: number): Observable<Employee> {
        
         const url = `${this.baseUrl}list/${id}/`;
         return this.http.get(url)
             .map(this.extractData)
             .do(data => console.log('getEmployee: ' + JSON.stringify(data)))
             .catch(this.handleError);
     }

     
    
     saveEmployee(employee: Employee): Observable<Employee> {
        //  let id:number=9
        //  employee.id = id
        //  employee.employeeDetail.id = id
        //  employee.professionByEmployee.id = id
        //  employee.employeeDetail.eId = id
        //  employee.professionByEmployee.eId = id 
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
            return this.createEmployee(employee, options);
    }

    updatingEmployee(employee: Employee): Observable<Employee> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.updateEmployee(employee, options);
    }

    private createEmployee(employee: Employee, options: RequestOptions): Observable<Employee> {
       
        return this.http.post(`${this.baseUrl}create/`, employee, options)
            .map(this.extractData)
            .do(data => console.log('createEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private updateEmployee(employee: Employee, options: RequestOptions): Observable<Employee> {
        console.log(employee.id)
        
        const url = `${this.baseUrl}list/${employee.id}/`;
        return this.http.put(url, employee, options)
            .map(() => employee)
            .do(data => console.log('updateEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteEmployee(id: number): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}list/${id}/`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteEmployee: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }



}
