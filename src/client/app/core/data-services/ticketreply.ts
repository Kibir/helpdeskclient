export class TicketReply{
    id: number
    ticketId: number
    responseType:string
    responseBody:string
    postedUserID:number
    postedUserName:string
    postedDateTime:string

  constructor(){
      
  }
}