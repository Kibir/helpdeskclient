import { Injectable, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';
import { Customer } from 'app/core/data-services/customer';


@Injectable()
export class CustomerService {
    private baseUrl = 'http://qshelpdeskapi.azurewebsites.net/api/customer/';

    constructor(private http: Http) { }

    getCustomerData(): Observable<Customer[]> {
        
        return this.http.get(`${this.baseUrl}list/`).debounceTime(400)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getSpecificCustomer(id: number): Observable<Customer> {
        
         const url = `${this.baseUrl}list/${id}/`;
         return this.http.get(url)
             .map(this.extractData)
             .do(data => console.log('getCustomer: ' + JSON.stringify(data)))
             .catch(this.handleError);
     }

     
    
     saveCustomer(customer: Customer): Observable<Customer> {
     
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
            return this.createCustomer(customer, options);
    }

    updatingCustomer(customer: Customer): Observable<Customer> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.updateCustomer(customer, options);
    }

    private createCustomer(customer: Customer, options: RequestOptions): Observable<Customer> {
       
        return this.http.post(`${this.baseUrl}create/`, customer, options)
            .map(this.extractData)
            .do(data => console.log('createCustomer: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private updateCustomer(customer: Customer, options: RequestOptions): Observable<Customer> {
        
        const url = `${this.baseUrl}list/${customer.id}/`;
        return this.http.put(url, customer, options)
            .map(() => Customer)
            .do(data => console.log('updateCustomer: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteCustomer(id: number): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}list/${id}/`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteCustomer: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }



}
