import { EmployeeDetail } from "app/core/data-services/employeeDetail";
import { ProfessionByEmployee } from "app/core/data-services/professionByEmployee";
import { Certificate } from "./certificate";
import { Degree } from "./degree";

export class Employee{
    id:number
    firstName:string
    lastName:string
    email:string
    password:string
    userRole:string
    employeeDetail:EmployeeDetail
    certificates:Certificate[]
    degrees:Degree[]
    
  constructor(){
      
  }
}