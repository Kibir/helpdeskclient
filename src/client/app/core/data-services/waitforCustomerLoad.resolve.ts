import { Injectable } from '@angular/core';
import {
    Router, Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { Customer } from 'app/core/data-services/customer';
import { CustomerService } from 'app/core/data-services/customer.service';

@Injectable()
export class WaitForCustomerLoad implements Resolve<Customer[]> {
    constructor(private customerService:CustomerService, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<Customer[]>  {
       
         return this.customerService.getCustomerData()
      
        };
    }
