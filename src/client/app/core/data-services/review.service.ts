import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';

import { Review } from './review';

@Injectable()
export class ReviewService {
    private baseUrl = 'http://qshelpdeskapi.azurewebsites.net/api/review/';
    
    constructor(private http: Http) { }

    getReviews(): Observable<Review[]> {
      
        return this.http.get(`${this.baseUrl}list/`).debounceTime(400)
            .map(this.extractData)
            .do(data => console.log('getreviews: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

   
    getReview(id: number): Observable<Review> {
       
        const url = `${this.baseUrl}list/${id}/`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getreview: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteReview(id: number): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}list/${id}/`;
        return this.http.delete(url, options)
            .do(data => console.log('deletereview: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveReview(review: Review): Observable<Review> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
            return this.createReview(review, options);
    }

    updatingReview(review: Review): Observable<Review> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.updateReview(review, options);
    }

    private createReview(review: Review, options: RequestOptions): Observable<Review> {
        review.id = 33;
        return this.http.post(`${this.baseUrl}create/`, review, options)
            .map(this.extractData)
            .do(data => console.log('createreview: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private updateReview(review: Review, options: RequestOptions): Observable<Review> {
     
        
        const url = `${this.baseUrl}list/${review.id}/`;
        return this.http.put(url, review, options)
            .map(() => review)
            .do(data => console.log('updatereview: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

 /*    initializeEvent(): ServiceEvent {
        
        return {
            id: 0,
            productName: null,
            productCode: null,
            tags: [''],
            releaseDate: null,
            price: null,
            description: null,
            starRating: null,
            imageUrl: null
        };
    } */
}
