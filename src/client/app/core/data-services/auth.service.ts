import { Injectable, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';

import { Employee } from 'app/core/data-services/employee';
import { SettingsService } from 'app/core/settings/settings.service';

@Injectable()
export class AuthService implements OnInit {
    private baseUrl = 'http://qshelpdeskapi.azurewebsites.net/api/employee/';
    private user:Employee
    private employeeList:Employee[]
    constructor(private http: Http, public settingsService:SettingsService) { }

    getUser(id: number): Observable<Employee> {
        
        const url = `${this.baseUrl}list/${id}/`;
        return this.http.get(url)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getEmployeeData(): Observable<Employee[]> {
        
        return this.http.get(`${this.baseUrl}list/`)
            .map(this.extractData)
            .catch(this.handleError);
    }

    ngOnInit(){
       
    }    

    setUser(user:Employee){
        this.settingsService.setUserSetting('John',user.firstName)
    }
    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

 
}
