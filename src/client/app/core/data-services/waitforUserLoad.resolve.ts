import { Injectable } from '@angular/core';
import {
    Router, Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { AuthService } from 'app/core/data-services/auth.service';

import { Employee } from 'app/core/data-services/employee';

@Injectable()
export class WaitforUserLoad implements Resolve<Employee[]> {
    constructor(private authService:AuthService, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<Employee[]>  {
       
         return this.authService.getEmployeeData()
      
        };
    }
