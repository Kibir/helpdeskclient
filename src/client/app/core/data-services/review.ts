export class Review{
    id: number
    ticketId:number
    description:string
    reviewerId:number
    reviewerName:string
    rating:number
    postedDateTime:string
    
  constructor(){
      
  }
}