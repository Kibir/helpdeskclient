import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';

import { Article } from './article';

@Injectable()
export class ArticleService {
    private baseUrl = 'http://qshelpdeskapi.azurewebsites.net/api/article/';
    
    constructor(private http: Http) { }

    getArticles(): Observable<Article[]> {
      
        return this.http.get(`${this.baseUrl}list/`).debounceTime(400)
            .map(this.extractData)
            .do(data => console.log('getArticles: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

   
    getArticle(id: number): Observable<Article> {
       
        const url = `${this.baseUrl}list/${id}/`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getArticle: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteArticle(id: number): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}list/${id}/`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteArticle: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveArticle(article: Article): Observable<Article> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
            return this.createArticle(article, options);
    }

    updatingArticle(article: Article): Observable<Article> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.updateArticle(article, options);
    }

    private createArticle(article: Article, options: RequestOptions): Observable<Article> {
        
        return this.http.post(`${this.baseUrl}create/`, article, options)
            .map(this.extractData)
            .do(data => console.log('createArticle: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private updateArticle(article: Article, options: RequestOptions): Observable<Article> {
     
        
        const url = `${this.baseUrl}list/${article.id}/`;
        return this.http.put(url, article, options)
            .map(() => Article)
            .do(data => console.log('updateArticle: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    uploadFile(fileList:FileList) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let headers = new Headers();

        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
       
        this.http.post(``, formData, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error))
            .subscribe(
                data => console.log('success'),
                error => console.log(error)
            )
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

 /*    initializeEvent(): ServiceEvent {
        
        return {
            id: 0,
            productName: null,
            productCode: null,
            tags: [''],
            releaseDate: null,
            price: null,
            description: null,
            starRating: null,
            imageUrl: null
        };
    } */
}
