import { Injectable } from '@angular/core';
import {
    Router, Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { TicketService } from 'app/core/data-services/ticket.service';
import { Ticket } from 'app/core/data-services/ticket';

@Injectable()
export class WaitForTicketLoad implements Resolve<Ticket[]> {
    constructor(private ticketService:TicketService, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<Ticket[]>  {
       
         return this.ticketService.getTickets()
        };
    }
