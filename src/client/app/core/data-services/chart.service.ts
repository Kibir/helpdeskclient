import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';

import { Ticket } from './ticket';

@Injectable()
export class ChartService {
    private baseUrl = 'api/spline';
    private baseUrl2 = 'api/splineV2';
    private baseUrl3 = 'api/barstackedV2';
    private baseUrl4 = 'api/splineV3';
    constructor(private http: Http) { }

    getChartData(): Observable<any[]> {
        
        return this.http.get(this.baseUrl)
            .map(this.extractData)
            .do(data => console.log('getChartDatas: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getChartData2(): Observable<any[]> {
        
        return this.http.get(this.baseUrl2)
            .map(this.extractData)
            .do(data => console.log('getChartDatas: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getChartData3(): Observable<any[]> {
        
        return this.http.get(this.baseUrl4)
            .map(this.extractData)
            .do(data => console.log('getChartDatas: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getBarData2(): Observable<any[]> {
        
        return this.http.get(this.baseUrl3)
            .map(this.extractData)
            .do(data => console.log('getChartDatas: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body.data || {};
    }

    private handleError(error: Response): Observable<any> {
        
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

 
}
