import { InMemoryDbService } from 'angular2-in-memory-web-api';

import { Ticket } from './ticket'
import { Employee } from './employee';
import { TicketReply } from './ticketreply';
import { Article } from 'app/core/data-services/article';
import { Customer } from 'app/core/data-services/customer';
import { Review } from 'app/core/data-services/review';


export class DataProvider implements InMemoryDbService {

  createDb() {
    let ticketsData: Ticket[] = [
      {
        'id': 1,
        'summary': ' Welcome to the Spiceworks Help Desk!',
        'description': 'Detailed description about this task and.....',
        'workSheetBody': 'Quam vulputate dignissim suspendisse in est ante. Massa enim nec dui nunc mattis enim ut tellus. Lobortis mattis aliquam faucibus purus. Magna fermentum iaculis eu non diam. Quis lectus nulla at volutpat diam ut. Velit ut tortor pretium viverra. Nunc lobortis mattis aliquam faucibus purus. In hac habitasse platea dictumst vestibulum. Turpis massa tincidunt dui ut ornare. Eget arcu dictum varius duis at consectetur lorem donec massa. Nulla posuere sollicitudin aliquam ultrices sagittis. Amet nisl suscipit adipiscing bibendum est. At erat pellentesque adipiscing commodo elit at imperdiet. Fermentum iaculis eu non diam phasellus. Amet nisl suscipit adipiscing bibendum est. Porttitor eget dolor morbi non arcu risus quis varius quam. Ultrices tincidunt arcu non sodales neque sodales. Semper eget duis at tellus. Risus quis varius quam quisque id diam vel quam. Nibh praesent tristique magna sit amet purus gravida quis.',
        'assignee': 'Thu Ra',
        'assigneeEID': 1,
        'acceptStatus': true,
        'category': 'Software',
        'creatorId': 4,
        'creator': 'Zeyar',
        'creatorRole': 'Admin',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'High',
        'dueDate': '2018-03-02T00:00:00Z',
        'lastUpdated': '2018-05-03T00:00:00Z',
        'created': '2018-05-03T00:00:00Z',
        'lastActivity': '5 Minutes ago',
        'status': 'Open',
        'progress': 50,
        'reasonForClosing':'',
        'reasonForReopening':''
      },
      {
        'id': 2,
        'summary': 'Task Two',
        'description': 'blahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': 'Commodo sed egestas egestas fringilla. Sociis natoque penatibus et magnis. Tellus id interdum velit laoreet id. Porttitor eget dolor morbi non arcu. Eleifend mi in nulla posuere sollicitudin aliquam. Pharetra sit amet aliquam id diam maecenas ultricies mi eget. Vitae tempus quam pellentesque nec nam aliquam sem et. Porttitor lacus luctus accumsan tortor posuere ac. Curabitur vitae nunc sed velit dignissim sodales ut eu. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at. Mauris pellentesque pulvinar pellentesque habitant morbi tristique. Vitae auctor eu augue ut lectus arcu bibendum at varius. Mollis nunc sed id semper. Facilisis volutpat est velit egestas dui id ornare arcu. Consectetur lorem donec massa sapien faucibus et. Arcu risus quis varius quam quisque.',
        'assignee': 'Zeyar',
        'assigneeEID': 4,
        'acceptStatus': true,
        'category': 'Software',
        'creatorId': 4,
        'creator': 'Zeyar',
        'creatorRole': 'Admin',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'High',
        'dueDate': '2018-03-02T00:00:00Z',
        'lastUpdated': '2018-05-03T00:00:00Z',
        'created': '2018-05-03T00:00:00Z',
        'lastActivity': '1 hour ago',
        'status': 'Open',
        'progress': 10,
        'reasonForClosing':'',
        'reasonForReopening':''
      },
      {
        'id': 3,
        'summary': 'Task Three',
        'description': 'blahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Morbi tempus iaculis urna id volutpat lacus. Venenatis tellus in metus vulputate eu scelerisque felis. Vivamus at augue eget arcu. Ac placerat vestibulum lectus mauris ultrices eros in cursus. Neque egestas congue quisque egestas diam in. Ipsum a arcu cursus vitae congue mauris rhoncus aenean vel. Aliquam id diam maecenas ultricies mi eget mauris. Habitasse platea dictumst vestibulum rhoncus est. Ut pharetra sit amet aliquam id. Amet risus nullam eget felis eget nunc lobortis mattis. Suscipit adipiscing bibendum est ultricies integer quis auctor elit. In est ante in nibh mauris cursus mattis. Odio tempor orci dapibus ultrices in iaculis nunc sed.',
        'assignee': 'PPA',
        'assigneeEID': 3,
        'acceptStatus': false,
        'category': 'Software',
        'creatorId': 4,
        'creator': 'Zeyar',
        'creatorRole': 'Admin',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'Medium',
        'dueDate': '2018-03-02T00:00:00Z',
        'lastUpdated': '2018-04-18T00:00:00Z',
        'created': '2018-04-18T00:00:00Z',
        'lastActivity': '54 Minutes ago',
        'status': 'Open',
        'progress': 36,
        'reasonForClosing':'',
        'reasonForReopening':''
      },
      {
        'id': 6,
        'summary': 'Task Four',
        'description': 'blahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': '“Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?',
        'assignee': 'PPA',
        'assigneeEID': 3,
        'acceptStatus': true,
        'category': 'Network',
        'creatorId': 4,
        'creator': 'Zeyar',
        'creatorRole': 'Admin',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'Low',
        'dueDate': '2018-03-02T00:00:00Z',
        'lastUpdated': '2018-05-03T00:00:00Z',
        'created': '2018-05-03T00:00:00Z',
        'lastActivity': '5 Minutes ago',
        'status': 'Reopened',
        'progress': 50,
        'reasonForClosing':'',
        'reasonForReopening':''
      },
      {
        'id': 7,
        'summary': 'Task Seven',
        'description': 'bdvsscszdawdclahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': '“Seddvszdvsdev ut fvbdxfperspiciatis, unde fbdfomnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?',
        'assignee': 'PPA',
        'assigneeEID': 3,
        'acceptStatus': true,
        'category': 'Email',
        'creatorId': 4,
        'creator': 'Zeyar',
        'creatorRole': 'Admin',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'Low',
        'dueDate': '2018-04-22T00:00:00Z',
        'lastUpdated': '2018-05-03T00:00:00Z',
        'created': '2018-05-03T00:00:00Z',
        'lastActivity': '5 Minutes ago',
        'status': 'Open',
        'progress': 20,
        'reasonForClosing':'',
        'reasonForReopening':''
      },
      {
        'id': 8,
        'summary': 'Task Eight',
        'description': 'hola hola bdvsscszdawdclahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': '“migratious edfsd dvSeddvszdvsdev ut fvbdxfperspiciatis, unde fbdfomnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?',
        'assignee': 'Zeyar',
        'assigneeEID': 4,
        'acceptStatus': true,
        'category': 'Other',
        'creatorId': 1,
        'creator': 'Thu Ra',
        'creatorRole': 'Normal',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'High',
        'dueDate': '2018-04-27T00:00:00Z',
        'lastUpdated': '2018-02-21T00:00:00Z',
        'created': '2018-02-21T00:00:00Z',
        'lastActivity': '52 Minutes ago',
        'status': 'Open',
        'progress': 0,
        'reasonForClosing':'',
        'reasonForReopening':''
      }, {
        'id': 9,
        'summary': 'Task Nine',
        'description': 'hola hola bdvsscszdawdclahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': '“migratious edfsd dvSeddvszdvsdev ut fvbdxfperspiciatis, unde fbdfomnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?',
        'assignee': 'Thu Ra',
        'assigneeEID': 1,
        'acceptStatus': true,
        'category': 'Other',
        'creatorId': 1,
        'creator': 'Thu Ra',
        'creatorRole': 'Normal',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'High',
        'dueDate': '2018-04-27T00:00:00Z',
        'lastUpdated': '2018-02-21T00:00:00Z',
        'created': '2018-02-21T00:00:00Z',
        'lastActivity': '52 Minutes ago',
        'status': 'Open',
        'progress': 20,
        'reasonForClosing':'',
        'reasonForReopening':''
      },
      {
        'id': 10,
        'summary': 'Task Ten',
        'description': 'bola bora hola hola bdvsscszdawdclahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': '“shadow king edfsd dvSeddvszdvsdev ut fvbdxfperspiciatis, unde fbdfomnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?',
        'assignee': 'Thu Ra',
        'assigneeEID': 1,
        'acceptStatus': true,
        'category': 'Other',
        'creatorId': 1,
        'creator': 'Thu Ra',
        'creatorRole': 'Normal',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'Normal',
        'dueDate': '2018-04-30T00:00:00Z',
        'lastUpdated': '2018-03-02T00:00:00Z',
        'created': '2018-02-21T00:00:00Z',
        'lastActivity': '45 Minutes ago',
        'status': 'Open',
        'progress': 30,
        'reasonForClosing':'',
        'reasonForReopening':''
      },
      {
        'id': 87,
        'summary': 'Task Five',
        'description': 'blahajkfcbsjkefckdn cskdlncdjlcns djfsd n',
        'workSheetBody': '“At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat…',
        'assignee': 'Lwin Maung Phyo',
        'assigneeEID': 2,
        'acceptStatus': true,
        'category': 'Software',
        'creatorId': 4,
        'creator': 'Zeyar',
        'creatorRole': 'Admin',
        'creatorCustomerId':null,
        'creatorCustomerName':null,
        'priority': 'Medium',
        'dueDate': '2018-03-02T00:00:00Z',
        'lastUpdated': '2018-05-03T00:00:00Z',
        'created': '2018-05-03T00:00:00Z',
        'lastActivity': '1 Day ago',
        'status': 'Open',
        'progress': 100,
        'reasonForClosing':'',
        'reasonForReopening':''
      }
    ];
    let spline: any[] = [{
      "label": "Uniques",
      "color": "#768294",
      "data": [
        ["Mar", 70],
        ["Apr", 85],
        ["May", 59],
        ["Jun", 93],
        ["Jul", 66],
        ["Aug", 86],
        ["Sep", 60]
      ]
    }, {
      "label": "Recurrent",
      "color": "#1f92fe",
      "data": [
        ["Mar", 21],
        ["Apr", 12],
        ["May", 27],
        ["Jun", 24],
        ["Jul", 16],
        ["Aug", 39],
        ["Sep", 15]
      ]
    }];
    let splineV2: any[] = [{
      "label": "Hours",
      "color": "#23b7e5",
      "data": [
        ["Jan", 70],
        ["Feb", 20],
        ["Mar", 70],
        ["Apr", 85],
        ["May", 59],
        ["Jun", 93],
        ["Jul", 66],
        ["Aug", 86],
        ["Sep", 60],
        ["Oct", 60],
        ["Nov", 12],
        ["Dec", 50]
      ]
    }, {
      "label": "Commits",
      "color": "#7266ba",
      "data": [
        ["Jan", 20],
        ["Feb", 70],
        ["Mar", 30],
        ["Apr", 50],
        ["May", 85],
        ["Jun", 43],
        ["Jul", 96],
        ["Aug", 36],
        ["Sep", 80],
        ["Oct", 10],
        ["Nov", 72],
        ["Dec", 31]
      ]
    }];
    let barstackedV2: any[] = [{
      "label": "Pending",
      "color": "#9289ca",
      "data": [
        ["Pj1", 86],
        ["Pj2", 136],
        ["Pj3", 97],
        ["Pj4", 110],
        ["Pj5", 62],
        ["Pj6", 85],
        ["Pj7", 115],
        ["Pj8", 78],
        ["Pj9", 104],
        ["Pj10", 82],
        ["Pj11", 97],
        ["Pj12", 110],
        ["Pj13", 62]
      ]
    }, {
      "label": "Assigned",
      "color": "#7266ba",
      "data": [
        ["Pj1", 49],
        ["Pj2", 81],
        ["Pj3", 47],
        ["Pj4", 44],
        ["Pj5", 100],
        ["Pj6", 49],
        ["Pj7", 94],
        ["Pj8", 44],
        ["Pj9", 52],
        ["Pj10", 17],
        ["Pj11", 47],
        ["Pj12", 44],
        ["Pj13", 100]
      ]
    }, {
      "label": "Completed",
      "color": "#564aa3",
      "data": [
        ["Pj1", 29],
        ["Pj2", 56],
        ["Pj3", 14],
        ["Pj4", 21],
        ["Pj5", 5],
        ["Pj6", 24],
        ["Pj7", 37],
        ["Pj8", 22],
        ["Pj9", 28],
        ["Pj10", 9],
        ["Pj11", 14],
        ["Pj12", 21],
        ["Pj13", 5]
      ]
    }];
    let splineV3: any[] = [{
      "label": "Home",
      "color": "#1ba3cd",
      "data": [
        ["1", 38],
        ["2", 40],
        ["3", 42],
        ["4", 48],
        ["5", 50],
        ["6", 70],
        ["7", 145],
        ["8", 70],
        ["9", 59],
        ["10", 48],
        ["11", 38],
        ["12", 29],
        ["13", 30],
        ["14", 22],
        ["15", 28]
      ]
    }, {
      "label": "Overall",
      "color": "#3a3f51",
      "data": [
        ["1", 16],
        ["2", 18],
        ["3", 17],
        ["4", 16],
        ["5", 30],
        ["6", 110],
        ["7", 19],
        ["8", 18],
        ["9", 110],
        ["10", 19],
        ["11", 16],
        ["12", 10],
        ["13", 20],
        ["14", 10],
        ["15", 20]
      ]
    }];
    let employeeData: Employee[] = [
      {
        'id': 0,
        'firstName': 'Choose Employee/Assignee',
        'lastName': '',
        'email': '',
        'password': '',
        'userRole': '',
        'employeeDetail': {
          'id': 0,
          'eId': 0,
          'departmentId': 0,
          'supervisor': '',
          'profession': '',
          'yearsInActive': 0,
          'levelofEducation': '',
          'NRC': ''
        },
        'certificates': [{ 'eId': 1, 'name': '' }],
        'degrees': [{ 'eId': 1, 'name': '' }]
      },
      {
        'id': 1,
        'firstName': 'Thu Ra',
        'lastName': ' Aung Kyaw',
        'email': 'thuraaungkyaw9@gmail.com',
        'password': '123456@a',
        'userRole': 'Normal',
        'employeeDetail': {
          'id': 1,
          'eId': 1,
          'departmentId': 1,
          'supervisor': 'Min Cho',
          'profession': 'Web Developer',
          'yearsInActive': 2,
          'levelofEducation': 'Under Graduate',
          'NRC': '12/BHN(N)107218'
        },
        'certificates': [{ 'eId': 1, 'name': '' }],
        'degrees': [{ 'eId': 1, 'name': '' }]
      }, {
        'id': 2,
        'firstName': 'Lwin Maung',
        'lastName': 'Phyo',
        'email': 'lwinmaungphyo@gmail.com',
        'password': '654321@b',
        'userRole': 'Normal',
        'employeeDetail': {
          'id': 2,
          'eId': 2,
          'departmentId': 1,
          'supervisor': 'Min Cho',
          'profession': 'Web Developer',
          'yearsInActive': 2,
          'levelofEducation': 'Under Graduate',
          'NRC': '12/LMD(N)123456'
        },
        'certificates': [{ 'eId': 1, 'name': '' }],
        'degrees': [{ 'eId': 1, 'name': '' }]
      }, {
        'id': 3,
        'firstName': 'PPA',
        'lastName': '',
        'email': 'ppa@gmail.com',
        'password': '123456@b',
        'userRole': 'Normal',
        'employeeDetail': {
          'id': 3,
          'eId': 3,
          'departmentId': 1,
          'supervisor': 'Min Cho',
          'profession': 'Web Developer',
          'yearsInActive': 3,
          'levelofEducation': 'Under Graduate',
          'NRC': '12/LMD(N)123456'
        },
        'certificates': [{ 'eId': 1, 'name': '' }],
        'degrees': [{ 'eId': 1, 'name': '' }]
      }, {
        'id': 4,
        'firstName': 'Zeyar',
        'lastName': '',
        'email': 'zeyar@gmail.com',
        'password': '654321@a',
        'userRole': 'Admin',
        'employeeDetail': {
          'id': 4,
          'eId': 4,
          'departmentId': 1,
          'supervisor': 'Min Cho',
          'profession': 'Senior Developer',
          'yearsInActive': 3,
          'levelofEducation': 'Post Graduate',
          'NRC': '12/LMD(N)123456'
        },
        'certificates': [{ 'eId': 1, 'name': '' }],
        'degrees': [{ 'eId': 1, 'name': '' }]
      }
    ];
    let responseData: TicketReply[] = [
      {
        'id': 1,
        'ticketId': 1,
        'responseType': 'Public',
        'responseBody': 'This ticket is needed to be resolved in a few weeks.',
        'postedUserID': 4,
        'postedUserName': 'Zeyar',
        'postedDateTime': '2018-03-02T00:00:00Z'
      }, {
        'id': 2,
        'ticketId': 1,
        'responseType': 'Private',
        'responseBody': 'You all have to look into this real quick.',
        'postedUserID': 1,
        'postedUserName': 'Thu Ra',
        'postedDateTime': '2018-08-02T00:00:00Z'
      }, {
        'id': 3,
        'ticketId': 2,
        'responseType': 'Private',
        'responseBody': 'This ticket is opened without providing enough information.',
        'postedUserID': 3,
        'postedUserName': 'PPA',
        'postedDateTime': '2018-08-02T00:00:00Z'
      }, {
        'id': 4,
        'ticketId': 2,
        'responseType': 'Private',
        'responseBody': 'This ticket is opened without providing enough information.',
        'postedUserID': 3,
        'postedUserName': 'PPA',
        'postedDateTime': '2018-08-02T00:00:00Z'
      }, {
        'id': 5,
        'ticketId': 2,
        'responseType': 'Public',
        'responseBody': 'This is a public ticket reply for ticket with the id of 2',
        'postedUserID': 1,
        'postedUserName': 'Thu Ra',
        'postedDateTime': '2018-08-02T00:00:00Z'
      }
    ];
    let articleData: Article[] = [
      {
        'id': 1,
        'title': 'Detect Unused Software on All Computers in Your Network',
        'body': 'Each computer in every office around the world has unnecessary, unwanted or unused software on it. Companies can save millions of dollars if they proactively detect and uninstall such shelfware. <br> This HOWTO helps you to detect unsused software on all windows computers in your network so you can uninstall it. <br>1. Get a List of Installed Software on a Remote Computer  <br> Run Powershell command (make sure you replace "RemoteComputerName" with the computer name you want to get the information from): Get-WmiObject -Namespace ROOT\CIMV2 -Class Win32_SoftwareFeature -Computer RemoteComputerName  <br> 2. Sort and Fiter Results  <br> It\'s natural to feel nervous when considering your first Microsoft certificate. Avoid the temptation to just dip your toes in the water by starting slow and easy. Working professionals don\'t need to bother with the MTA. It\'s not an adequate introduction to the exam process nor does it provide any substantial benefit in prospective employment. Sitting for the MTA still requires preparation and payment; both of which are better redirected toward a certification that can yield measurable benefits. It CAN be useful for high school or college students who are lacking in sufficient professional experience and need a credential in order to apply for an internship.  <br> 3. Query Multiple Computers <br> Before entering a configuration that could disrupt your network traffic you would enter the reload command and add a time for it to reload. There are a few options as to how you can enter the time for the reload. The easiest method is to run from priv exec mode \'reload in XX\'. Substitute the XX for the number of minutes until the reload will execute. Be sure to allow yourself enough time to enter and test the configuration you are wanting to apply..',
        'conclusionBody': 'This is the conclusion. This is the conclusion. This is the conclusion. This is the conclusion. This is the conclusion.',
        'postedDate': '2018-03-02T00:00:00Z',
        'lastUpdatedDate': '2018-03-02T00:00:00Z',
        'postedUserId': 1,
        'postedUserName': 'Thu Ra',
        'references': [{
          'articleId': 1,
          'refName': 'Custom Reference',
          'refLink': 'https://www.google.com'
        }]
      },
      {
        'id': 2,
        'title': 'Article 2',
        'body': 'Powerful and modern routes definitions. Easily declare routes and nested routes with components association.',
        'conclusionBody': 'srfvvsdgrvdrvgdsdfsdftring',
        'postedDate': '2018-03-02T00:00:00Z',
        'lastUpdatedDate': '2018-03-02T00:00:00Z',
        'postedUserId': 1,
        'postedUserName': 'Thu Ra',
        'references': [{
          'articleId': 2,
          'refName': 'Custom Reference',
          'refLink': 'https://www.twitter.com'
        }]
      },
      {
        'id': 3,
        'title': 'Article 3',
        'body': 'Angular-CLI tool provides generators to create components, services, directives and pipe from the command line.',
        'conclusionBody': 'srfwsfaswefq3r345454gdsdfsdftring',
        'postedDate': '2018-03-02T00:00:00Z',
        'lastUpdatedDate': '2018-03-02T00:00:00Z',
        'postedUserId': 3,
        'postedUserName': 'PPA',
        'references': [{
          'articleId': 3,
          'refName': 'Custom Reference',
          'refLink': 'https://www.facebook.com'
        }]
      },
      {
        'id': 4,
        'title': 'Article 4',
        'body': 'This project has been carefully designed to provide modules for Core, Layout, Shared and Routed components.',
        'conclusionBody': 'art4srfwergegraesfaswefq3r345454gdsdfsdftring',
        'postedDate': '2018-03-02T00:00:00Z',
        'lastUpdatedDate': '2018-03-02T00:00:00Z',
        'postedUserId': 2,
        'postedUserName': 'Lwin Maung',
        'references': [{
          'articleId': 4,
          'refName': 'Custom Reference',
          'refLink': 'https://www.facebook.com'
        }]
      },
      {
        'id': 5,
        'title': 'Article 5',
        'body': 'Ready to work with multiple environments, from first stage development through test and production.Better practices for Code organization and Project structure based on the official Angular2 style guide.Better practices for Code organization and Project structure based on the official Angular2 style guide.',
        'conclusionBody': 'art5srfwergegraesfaswefq3r345454gdsdfsdftring',
        'postedDate': '2018-03-02T00:00:00Z',
        'lastUpdatedDate': '2018-03-02T00:00:00Z',
        'postedUserId': 4,
        'postedUserName': 'Zeyar',
        'references': [{
          'articleId': 5,
          'refName': 'Custom Reference',
          'refLink': 'https://www.mail.google.com'
        }]
      },
      {
        'id': 6,
        'title': 'Article 6',
        'body': 'Better practices for Code organization and Project structure based on the official Angular2 style guide.Better practices for Code organization and Project structure based on the official Angular2 style guide.Better practices for Code organization and Project structure based on the official Angular2 style guide.',
        'conclusionBody': 'art5srfwevsdrvsdsfdsdvrgegraesfaswefq3r345454gdsdfsdftring',
        'postedDate': '2018-03-02T00:00:00Z',
        'lastUpdatedDate': '2018-03-02T00:00:00Z',
        'postedUserId': 4,
        'postedUserName': 'Zeyar',
        'references': [{
          'articleId': 6,
          'refName': 'Custom Reference',
          'refLink': 'https://www.instagram.com'
        }]
      }
    ];
    let customerData: Customer[] = [
      {
        'id': 1,
        'name': 'Thu Ra Aung Kyaw',
        'email': 'thuraaungkyaw9@gmail.com',
        'company': 'QSLogics',
        'password': 'Mysterious9',
      },
      {
        'id': 2,
        'name': 'Lwin Maung Phyo',
        'email': 'lmpcc@gmail.com',
        'company': 'QSLogics',
        'password': '123456@aA',
      },
      {
        'id': 3,
        'name': 'QSLogics',
        'email': 'qslogics@gmail.com',
        'company': 'QSLogics',
        'password': '123456@aA',
      }
    ];
    let reviewData: Review[] = [
      {
        'id': 1,
        'ticketId': 1,
        'description': 'Even though it is not completely resolved, I\'m just glad that I got the response pretty fast',
        'reviewerId': 3,
        'reviewerName': 'QSLogics',
        'rating': 4,
        'postedDateTime': '2018-03-02T00:00:00Z'
      },
      {
        'id': 2,
        'ticketId': 2,
        'description': 'Haven\'t gotten any response about this case. It\'s been a while now',
        'reviewerId': 3,
        'reviewerName': 'QSLogics',
        'rating': 1,
        'postedDateTime': '2018-03-02T00:00:00Z'
      },
      {
        'id': 3,
        'ticketId': 3,
        'description': 'There\'s too many complications about this case. I want to reopen it.',
        'reviewerId': 3,
        'reviewerName': 'QSLogics',
        'rating': 3,
        'postedDateTime': '2018-03-02T00:00:00Z'
      },
      {
        'id': 4,
        'ticketId': 87,
        'description': 'Wow. A superb solution without even taking much time!',
        'reviewerId': 3,
        'reviewerName': 'QSLogics',
        'rating': 5,
        'postedDateTime': '2018-03-02T00:00:00Z'
      }
    ]
    return { ticketsData, spline, splineV2, splineV3, barstackedV2, employeeData, responseData, articleData, customerData, reviewData };
  }
}
