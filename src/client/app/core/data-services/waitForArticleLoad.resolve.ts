import { Injectable } from '@angular/core';
import {
    Router, Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Article } from 'app/core/data-services/article';
import { ArticleService } from 'app/core/data-services/article.service';



@Injectable()
export class WaitForArticleLoad implements Resolve<Article[]> {
    constructor(private articleService:ArticleService, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<Article[]>  {
       
         return this.articleService.getArticles()
      
        };
    }
