import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/finally';

import {TicketReply} from './ticketreply'

@Injectable()
export class TicketReplyService {
    private baseUrl = 'http://qshelpdeskapi.azurewebsites.net/api/ticket-activity/';
    
    constructor(private http: Http) { }

    getTicketReplies(): Observable<TicketReply[]> {
      
        return this.http.get(`${this.baseUrl}list/`).debounceTime(400)
            .map(this.extractData)
            .do(data => console.log('getTicketReplies: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

   
    getTicket(id: number): Observable<TicketReply> {
       
        const url = `${this.baseUrl}list/${id}/`;
        return this.http.get(url)
            .map(this.extractData)
            .do(data => console.log('getTicketReply: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    deleteTicket(id: number): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const url = `${this.baseUrl}list/${id}/`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteTicketReply: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    saveTicketReply(ticketReply: TicketReply): Observable<TicketReply> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
            return this.createTicketReply(ticketReply, options);
    }

    updatingTicket(ticketReply: TicketReply): Observable<TicketReply> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.updateTicketReply(ticketReply, options);
    }

    private createTicketReply(ticketReply: TicketReply, options: RequestOptions): Observable<TicketReply> {
        // ticketReply.id = 33;
        return this.http.post(`${this.baseUrl}create/`, ticketReply, options)
            .map(this.extractData)
            .do(data => console.log('createTicketReply: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private updateTicketReply(ticketReply: TicketReply, options: RequestOptions): Observable<TicketReply> {
    
        const url = `${this.baseUrl}list/${ticketReply.id}/`;
        return this.http.put(url, ticketReply, options)
            .map(() => ticketReply)
            .do(data => console.log('updateTicketReply: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private extractData(response: Response) {
        let body = response.json();
        return body || {};
    }

    private handleError(error: Response): Observable<any> {

        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
