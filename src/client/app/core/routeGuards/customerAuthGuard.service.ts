import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserblockService } from '../../layout/sidebar/userblock/userblock.service';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public ubs: UserblockService, public router: Router) {}
  canActivate(): boolean {
    if (this.ubs.getUserData().role == "Customer" || this.ubs.getUserData().role == "Guest") {
      this.router.navigate(['home']);
      return false;
    }
    return true;
  }
}