import { Injectable, OnInit } from '@angular/core';
import { UserblockService } from 'app/layout/sidebar/userblock/userblock.service';

@Injectable()
export class MenuService {
    reloadStatus: boolean = false
    menuItems: Array<any>;

    constructor(private u: UserblockService) {
        this.menuItems = [];
    }

    addMenu(items: Array<{
        text: string,
        heading?: boolean,
        link?: string,
        elink?: string,
        target?: string,
        icon?: string,
        alert?: string,
        submenu?: Array<any>
    }>) {
        this.menuItems = [];
        items.forEach((item) => {
            if (this.u.getUserData().id > 0 && this.u.getUserData().role != 'Guest' && this.u.getUserData().role != 'Customer') {

                if (this.menuItems.indexOf(item) < 0) {
                    this.menuItems.push(item);
                }

            } else if (this.u.getUserData().role == 'Guest' || this.u.getUserData().role == 'Customer') {

                if (item.text != 'Dashboard' && item.text != 'Tickets' && item.text != 'Contacts' && item.text != 'Employees' &&
                    item.text != 'Knowledge Base' && item.text != 'Widgets') {
                    this.menuItems.push(item);
                }
            }
        });

        if (this.u.getUserData().role != 'Customer' && this.u.getUserData().role != 'Guest' && this.menuItems[1].text == 'Home') {
            this.menuItems.splice(1, 1)
        }
    }

    getMenu() {
        return this.menuItems;
    }

}
